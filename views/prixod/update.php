<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prixod */

$this->title = 'Приходни тахрирлаш: '.$model->dori->name;
$this->params['breadcrumbs'][] = ['label' => 'Приход', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dori->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Тахрирлаш';
?>
<div class="prixod-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
