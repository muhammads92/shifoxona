<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Prixod */

$this->title = 'Приход қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Приход', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prixod-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
