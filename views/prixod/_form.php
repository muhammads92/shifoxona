<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Prixod */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prixod-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dori_id')->dropDownList(\app\models\Dori::all()) ?>

    <?= $form->field($model, 'amound')->textInput() ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Dori qo`shilgan sanani kiriting'],
        'pluginOptions' => [
            'todayHighlight' => true,
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy',
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
