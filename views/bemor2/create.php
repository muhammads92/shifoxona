<?php

use yii\helpers\Html;



/* @var $this yii\web\View */
/* @var $model app\models\Bemor */

$this->title = 'Бемор қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Беморлар', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bemor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <p>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'], ['class' => 'btn btn-primary pull-right']) ?>
    </p>

</div>
