<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BemorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Беморлар';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bemor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Бемор қўшиш', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fio',
            'manzil',
            'descraption:ntext',
            [
                'attribute' => 'active',
                'value' => function($model){
                    return [1=>'Ҳа', 0=>'Йўқ'][$model->active];
                },
                'filter' => Html::activeDropDownList($searchModel, 'active', [NULL=>NULL, 1=>'Ҳа', 0=>'Йўқ']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
