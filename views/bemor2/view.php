<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Bemor */

$this->title = $model->fio;
$this->params['breadcrumbs'][] = ['label' => 'Беморлар', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bemor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Тахрирлаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ўчиришга ишончингиз комилми?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Даволаниш қўшиш', ['/davolanish/create',], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fio',
            'manzil',
            'descraption:ntext',
            [
                'attribute' => 'active',
                'value' => function($model){
                    return [1=>'Ҳа', 0=>'Йўқ'][$model->active];
                },
            ],
        ],
    ]) ?>

</div>
