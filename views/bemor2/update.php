<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bemor */

$this->title = 'Беморни тахрирлаш: '.$model->fio;
$this->params['breadcrumbs'][] = ['label' => 'Беморлар', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fio, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Тахрирлаш';
?>
<div class="bemor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
