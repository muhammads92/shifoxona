<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model app\models\Bemor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bemor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'manzil')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descraption')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'active')->widget(CheckboxX::classname(), [
        'pluginOptions'=>['threeState'=>false],
        'autoLabel'=>true
        ])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
