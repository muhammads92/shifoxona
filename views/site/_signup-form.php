<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php echo Html::beginForm('/user/signup', '', ['id' => 'signup-form']) ?>
    <table>
        <tbody>
            <tr>
                <td width="80px"><?= Html::activeLabel($model, 'username')?></td>
                <td>:</td>
                <td><?= Html::activeTextInput($model, 'username')?></td>
            </tr>
            <tr>
                <td width="80px"><?= Html::activeLabel($model, 'email')?></td>
                <td>:</td>
                <td><?= Html::activeTextInput($model, 'email')?></td>
            </tr>
            <tr>
                <td width="80px"><?= Html::activeLabel($model, 'password')?></td>
                <td>:</td>
                <td><?= Html::activePasswordInput($model, 'password')?></td>
            </tr>
        </tbody>
    </table>

<?php echo Html::endForm()?>
