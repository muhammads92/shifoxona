<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?=Html::beginForm(['site/login'],'',['id'=>'login-form'])?>
    <table width="100%">
        <tbody>
            <tr>
                <td colspan="3"><i>Please enter your username and password</i></td>
            </tr>
            <tr>
                <td width="80px"><?= Html::activeLabel($model, 'username')?></td>
                <td>:</td>
                <td><?= Html::activeTextInput($model, 'username')?></td>
            </tr>
            <tr>
                <td><?= Html::activeLabel($model, 'password')?></td>
                <td>:</td>
                <td><?= Html::activePasswordInput($model, 'password')?></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><?= Html::activeCheckbox($model, 'rememberMe') ?></td>
            </tr>
            <!--<tr>
                <td colspan="3">
                    <a id="login-forgot-btn" href="javascript:void(0);">Forgot Password</a>, or 
                    <a id="login-signup-btn" href="javascript:void(0);">SignUp</a></td>
            </tr>-->
        </tbody>
    </table>
<?=Html::endForm()?>
