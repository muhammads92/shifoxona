<?php

/* @var $this yii\web\View */

$this->title = 'Шифохона';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Шифохона.</h1>

        <p class="lead">Шифохона хақидаги маълумотлар.</p>

        <p><a class="btn btn-lg btn-success" href="/web/bemor">Беморлар рўйхати</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Касаллик турлари</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-primary" href="/web/davo">Касаллик турлари &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Тўлов қилганлар</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-primary" href="/web/tolov">Тўлов қилганлар &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Даволанганлар</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-primary" href="/web/davolanish">Даволанганлар &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
