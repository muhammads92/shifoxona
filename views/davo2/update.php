<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Davo */

$this->title = 'Кассалик турини тахрирлаш: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Касаллик тури', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Тахрирлаш';
?>
<div class="davo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
