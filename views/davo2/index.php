<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DavoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Касаллик тури';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="davo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Касаллик турини қўшиш', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                    'attribute' => 'summa',
                    'format' => ['decimal', 0],
                    'contentOptions' => ['style'=>'text-align:right'],
            ],
            'descraption:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
