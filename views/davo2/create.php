<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Davo */

$this->title = 'Касаллик турини қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Касаллик тури', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="davo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
