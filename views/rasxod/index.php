<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RasxodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Расходлар';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rasxod-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Расход қўшиш', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'dori_id',
                'value' => function($model){
                    return $model->dori->name;
                }
            ],
            'amound',
            [
                'attribute' => 'date',
                'filter' => DateRangePicker::widget([
//                    'options' => ['style' => 'width:200px; display:inline-block;', 'class'=>'form-control'],
                    'model'=>$searchModel,
                    'attribute'=>'dateRange',
                    'convertFormat'=>true,
                    'useWithAddon'=>false,
                    'pluginOptions'=>[
                        'linkedCalendars' => true,
                        'locale' => [
                            'format'=>'d.m.Y',
                        ],
                        'separator'=>' - ',
                        'opens'=>'left',
                    ]
                ]),
                'value' => 'date',
                'format' => 'date',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
