<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rasxod */

$this->title = 'Расходни тахрирлаш: '.$model->dori->name;
$this->params['breadcrumbs'][] = ['label' => 'Расход', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dori->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Тахрирлаш';
?>
<div class="rasxod-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
