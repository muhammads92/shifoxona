<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Rasxod */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rasxod-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dori_id')->dropDownList(\app\models\Dori::all()) ?>

    <?= $form->field($model, 'amound')->textInput() ?>

    <?php echo $form->field($model, 'date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Dori qo`shilgan sanani kiriting'],
        'pluginOptions' => [
            'todayHighlight' => true,
            'autoclose'=>true,
            'format' => 'yyyy.mm.dd',
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
