<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rasxod */

$this->title = 'Расход қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Расход', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rasxod-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
