<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rasxod */

$this->title = $model->dori->name;
$this->params['breadcrumbs'][] = ['label' => 'Расход', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rasxod-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Тахрирлаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ўчиришга ишончингиз комилми?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'dori_id',
                'value' => function($model){
                    return $model->dori->name;
                }
            ],
            'amound',
            [
                'attribute' => 'date',
                'format' => 'date',
            ]
        ],
    ]) ?>

</div>
