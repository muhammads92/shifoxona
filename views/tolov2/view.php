<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tolov */

$this->title = $model->davolanish->bemor->fio.' / '.$model->davolanish->date;
$this->params['breadcrumbs'][] = ['label' => 'Тўловлар', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tolov-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Тахрирлаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ушбу маълумотни ўчиришга ишончингиз комилми?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'davolanish_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->davolanish->bemor->fio.' / '.$model->davolanish->date;
                },
            ],
            'summa',
            'date',
            'discription:ntext',
        ],
    ]) ?>

</div>
