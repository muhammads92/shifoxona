<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Tolov */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tolov-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'davolanish_id')->dropDownList(app\models\Davolanish::all()) ?>

    <?= $form->field($model, 'summa')->textInput() ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Sanani kiriting'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true,
        ]
    ]); ?>

    <?= $form->field($model, 'discription')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
