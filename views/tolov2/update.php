<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tolov */

$this->title = 'Тўловни тахрирлаш: '.$model->davolanish->bemor->fio.' / '.$model->davolanish->date;
$this->params['breadcrumbs'][] = ['label' => 'Тўловлар', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->davolanish->bemor->fio, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Тахрирлаш';
?>
<div class="tolov-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
