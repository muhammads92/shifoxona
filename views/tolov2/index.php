<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TolovSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тўловлар';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tolov-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Тўлов қўшиш', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'davolanish_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->davolanish->bemor->fio;
                },
            ],
            [
                'attribute' => 'date_davolanish',
                'filter' => DateRangePicker::widget([
//                    'options' => ['style' => 'width:200px; display:inline-block;', 'class'=>'form-control'],
                    'model'=>$searchModel,
                    'attribute'=>'dateRange',
                    'convertFormat'=>true,
                    'useWithAddon'=>false,
                    'pluginOptions'=>[
                        'linkedCalendars' => true,
                        'locale' => [
                            'format'=>'d.m.Y',
                        ],
                        'separator'=>' - ',
                        'opens'=>'right',
                    ]
                ]),
                'value' => function($model){
                    return $model->davolanish->date;
                },
                'format' => 'date',
            ],
            [
                'attribute' => 'came_from',
                'format' => 'raw',
                'value' => function($model){
                    return $model->davolanish->came_from;
                },
            ],
            [
                'attribute' => 'summa',
                'format' => ['decimal', 0],
                'contentOptions' => ['style'=>'text-align:right'],
            ],
            [
                'attribute' => 'date',
                'filter' => DateRangePicker::widget([
//                    'options' => ['style' => 'width:200px; display:inline-block;', 'class'=>'form-control'],
                    'model'=>$searchModel,
                    'attribute'=>'dateRange1',
                    'convertFormat'=>true,
                    'useWithAddon'=>false,
                    'pluginOptions'=>[
                        'linkedCalendars' => true,
                        'locale' => [
                            'format'=>'d.m.Y',
                        ],
                        'separator'=>' - ',
                        'opens'=>'left',
                    ]
                ]),
                'value' => 'date',
                'format' => 'date',
            ],
            'discription:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
