<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tolov */

$this->title = 'Тўлов қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Тўловлар', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tolov-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
