<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Batafsil */

$davolanish_id = $davolanish->id;

$this->title = 'Батафсил қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Даволаниш', 'url' => ['/../davolanish']];
$this->params['breadcrumbs'][] = ['label' => 'Батафсил', 'url' => ['index?davolanish_id='.$davolanish_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="batafsil-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'davolanish_id'=>$davolanish->id
    ]) ?>

</div>
