<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Batafsil */

$this->title = $model->davolanish->bemor->fio;
$this->params['breadcrumbs'][] = ['label' => 'Даволаниш', 'url' => ['/../davolanish']];
$this->params['breadcrumbs'][] = ['label' => 'Батафсил', 'url' => ['index', 'davolanish_id'=>$model->davolanish_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="batafsil-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Тахрирлаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ўчиришга ишончингиз комилми?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'davolanish_id',
                'value' => function($model){
                    return $model->davolanish->bemor->fio;
                }
            ],
            [
                'attribute' => 'davo_id',
                'value' => function($model){
                    return $model->davo->name;
                }
            ],
            'narxi',
            'description:ntext',
        ],
    ]) ?>

</div>
