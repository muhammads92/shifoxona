<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BatafsilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Батафсил';
$this->params['breadcrumbs'][] = ['label' => 'Даволаниш', 'url' => ['/../davolanish']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="batafsil-index">

    <?php $davolanish->date = date("d.m.Y"); ?>
    <h3><?= $davolanish->bemor->fio .' / '. $davolanish->date.' / '.$davolanish->came_from ;
    ?></h3>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Даволанганни қўшиш', ['create', 'davolanish_id' => $davolanish->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'davo_id',
                'value' => function($model){
                return $model->davo->name;
            }
            ],
            [
                'attribute' => 'narxi',
                'format' => ['decimal', 0],
                'contentOptions' => ['style'=>'text-align:right'],
            ],
            'description:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
