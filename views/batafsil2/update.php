<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Batafsil */


$this->title = 'Батафсил тахрирлаш: '.$model->davolanish->bemor->fio;
$this->params['breadcrumbs'][] = ['label' => 'Батафсил', 'url' => ['index', 'davolanish_id' => $model->davolanish_id]];
$this->params['breadcrumbs'][] = ['label' => $model->davolanish->bemor->fio, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Тахрирлаш';
?>
<div class="batafsil-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'davolanish_id' =>$davolanish->id,
    ]) ?>

</div>
