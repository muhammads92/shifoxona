<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Batafsil */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="batafsil-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::activeHiddenInput($model, 'davolanish_id', ['value' => $davolanish_id])  ?>

    <?= $form->field($model, 'davo_id')->dropDownList(\app\models\Davo::all()) ?>

    <?= $form->field($model, 'narxi')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
