<?php

use app\assets\AppAsset;
use yii\helpers\Html;
use app\components\helpers\Regex;
use yii\helpers\Url;
use yii\helpers\Json;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <div id="global-error">asd</div>
        <?php $this->beginBody() ?>

        <?php $this->endBody() ?>
    </body>
</html>

<?php
$username = Yii::$app->user->identity->username;
$logoutUrl = Url::to(['site/logout'],true);
$northContent = preg_replace(Regex::htmlMinified, ' ', $this->render('_north-content'));
$centerContent = '<div id="maintab"></div>';
$westContent = preg_replace(Regex::htmlMinified, ' ', $this->render('_west-content'));

$params = $this->params;

require(__DIR__ . '/_nav-item.php');

$this->params['selectedNavAccordion'] = isset($this->params['selectedNavAccordion']) ? $this->params['selectedNavAccordion'] : 'dashboard';

$navItem[$this->params['selectedNavAccordion']]['selected'] = true;

$navItemJson = Json::encode($navItem);

$this->registerJs(<<<EOD
    yii.app.logoutUrl = '{$logoutUrl}';
    yii.app.northContent = '{$northContent}';
    yii.app.centerContent = '{$centerContent}';
    yii.app.westContent = '{$westContent}';
    yii.app.navItem = {$navItemJson};
    yii.app.selectedNav = '{$this->params['selectedNav']}';
    yii.app.init();
EOD
);?>

<?php $this->endPage(); ?>
