<?php
use yii\helpers\Url;

$navItemUrl = [
    'dashboard' =>[
        'dashboard' => Url::to(['site/index'], true)
    ],
    'tolov' =>[
        'list' =>Url::to(['tolov/index'],true),
        'new' =>Url::to(['tolov/new'],true)
    ]
];
$navItem = [
    'dashboard'=>[
        'title'=>'Dashboard',
        'iconCls'=>'icon-tip',
        'content'=><<<HTML
            <a id="nav-dashboard" class="nav-btn" data-icon="icon-help" data-url="{$navItemUrl['dashboard']['dashboard']}" data-tabtitle="Dashboard">Dashboard</a>
HTML
    ]
    ,'tolov' =>[
        'title' =>'Tolov',
        'iconCls' =>'icon-tip',
        'content' =><<<HTML
            <a id="nav-tolov-list" class="nav-btn" data-icon="icon-help" data-url="{$navItemUrl['tolov']['list']}" data-tabtitle="Tolov List">List</a>
            <a id="nav-tolov-new" class="nav-btn" data-icon="icon-help" data-url="{$navItemUrl['tolov']['new']}" data-tabtitle="Tolov New">New</a>
HTML
    ]
];