<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Davolanish */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="davolanish-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Sanani kiriting'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true,
        ]
    ]); ?>

    <p>
        <?= Html::a('Бемор қўшиш', ['/../bemor/create'], ['class' => 'btn btn-primary pull-right']) ?>
    <br></p>

    <?= $form->field($model, 'came_from')->textarea(['rows' => 5]) ?>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-list-alt"></i> Batafsil</h4></div>
        <div class="panel-body">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 40, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelsBatafsil[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'davolanish_id',
                    'davo_id',
                    'narxi',
                    'desciption',
                ],
            ]); ?>

    <div class="container-items"><!-- widgetContainer -->
        <?php foreach ($modelsBatafsil as $i => $modelBatafsil): ?>
            <div class="item panel panel-default"><!-- widgetBody -->
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Batafsil</h3>
                    <div class="pull-right">
                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?php
                    // necessary for update action.
                    if (! $modelBatafsil->isNewRecord) {
                        echo Html::activeHiddenInput($modelBatafsil, "[{$i}]id");
                    }
                    ?>
                    <?= Html::activeHiddenInput($modelBatafsil, "[{$i}]davolanish_id", ['value'=>$model->id]); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <?= $form->field($modelBatafsil, "[{$i}]davo_id")->dropDownList(\app\models\Davo::all()) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($modelBatafsil, "[{$i}]narxi")->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($modelBatafsil, "[{$i}]description")->textarea(['maxlength' => true, 'rows' => 1]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php DynamicFormWidget::end(); ?>
</div>
</div>
</div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
