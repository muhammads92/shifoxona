<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Davolanish */

$this->title = 'Даволанишни тахрирлаш: '.$model->bemor->fio;
$this->params['breadcrumbs'][] = ['label' => 'Даволаниш', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bemor->fio, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Тахрирлаш';
?>
<div class="davolanish-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsBatafsil' => $modelsBatafsil,
    ]) ?>

</div>
