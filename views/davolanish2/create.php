<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Davolanish */

$this->title = 'Даволаниш турини қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Даволаниш', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="davolanish-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsBatafsil' => $modelsBatafsil,
    ]) ?>


</div>
