<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DavolanishSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Даволаниш';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="davolanish-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Даволаниш турини қўшиш', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                    'attribute' => 'date',
                    'filter' => DateRangePicker::widget([
                    'options' => ['style' => 'width:200px; display:inline-block;', 'class'=>'form-control'],
                    'model'=>$searchModel,
                    'attribute'=>'dateRange',
                    'convertFormat'=>true,
                    'useWithAddon'=>false,
                    'pluginOptions'=>[
                        'linkedCalendars' => true,
                        'locale' => [
                            'format'=>'d.m.Y',
                        ],
                        'separator'=>' - ',
                        'opens'=>'right',
                    ]
                ]),
                'format' => 'date',
                'value' => 'date',
            ],
            [
                'attribute' => 'bemor_id',
                'format' => 'raw',
                'value' => function($model){
                    return '<span title="'.$model->bemor->manzil.'">'.$model->bemor->fio.'</span>';
                },
            ],
            'came_from',
            [
                'attribute' => 'summa',
                'format' => ['decimal', 0],
                'contentOptions' => ['style'=>'text-align:right'],
                'value' => function($model){
                    return $model->summa;
                },
            ],
            [
                'attribute' => 'tolov',
                'format' => ['decimal', 0],
                'contentOptions' => ['style'=>'text-align:right'],
                'value' => function($model){
                    return $model->tolov;
                },
            ],
            [
                'attribute' => 'qarz',
                'format' => ['decimal', 0],
                'contentOptions' => ['style'=>'text-align:right'],
                'value' => function($model){
                    return $model->qarz;
                },
            ],
            [

                'format' => 'raw',
                'value' => function($model){
                    return '<a href="'.Url::to(['/batafsil/index', 'davolanish_id' => $model->id]).'" title = "Batafsil">
                    <span class="glyphicon glyphicon-list"></span></a>';
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
