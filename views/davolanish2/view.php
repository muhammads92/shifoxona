<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Davolanish */

$this->title = $model->bemor->fio;
$this->params['breadcrumbs'][] = ['label' => 'Даволаниш', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="davolanish-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Тахрирлаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ушбу маълумотни ўчиришга ишончингиз комилми?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'date',
                'format' => 'date',
            ],
            [
                'attribute' => 'bemor_id',
                'value' => function($model){
                    return $model->bemor->fio;
                }
            ],
            'came_from',
        ],
    ]) ?>


</div>
