<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Дорилар';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dori-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Дори қўшиш', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'descraption:ntext',
            [
                'attribute' => 'summa',
                'format' => ['decimal', 0],
                'contentOptions' => ['style' => 'text-align:right'],
            ],
            [
                'attribute' => 'balance',
                'value' => function($model){
                    return $model->balance;
                },
            ],
            [
                'attribute' => 'tsumma',
                'format' => ['decimal', 0],
                'contentOptions' => ['style' => 'text-align:right'],
                'value' => function($model){
                    return $model->balance * $model->summa;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
