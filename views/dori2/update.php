<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dori */

$this->title = 'Дорини тахрирлаш: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Дорилар', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Тахрирлаш';
?>
<div class="dori-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
