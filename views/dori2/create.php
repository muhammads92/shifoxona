<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Dori */

$this->title = 'Дори қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Дорилар', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dori-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
