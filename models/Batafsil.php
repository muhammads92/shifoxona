<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "batafsil".
 *
 * @property int $id
 * @property int $davolanish_id
 * @property int $davo_id
 * @property int $narxi
 * @property string $description
 *
 * @property Davolanish $davolanish
 * @property Davo $davo
 */
class Batafsil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'batafsil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['davo_id', 'narxi'], 'required'],
            [['davolanish_id', 'davo_id', 'narxi'], 'integer'],
            [['description'], 'string'],
            [['davolanish_id'], 'exist', 'skipOnError' => true, 'targetClass' => Davolanish::className(), 'targetAttribute' => ['davolanish_id' => 'id']],
            [['davo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Davo::className(), 'targetAttribute' => ['davo_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'davolanish_id' => 'Бемор Ф.И.Ш.',
            'davo_id' => 'Касаллик тури',
            'narxi' => 'Нархи',
            'description' => 'Маълумот',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDavolanish()
    {
        return $this->hasOne(Davolanish::className(), ['id' => 'davolanish_id']);
    }
    public function getBemor()
    {
        return $this->hasOne(Bemor::className(), ['id' => 'bemor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDavo()
    {
        return $this->hasOne(Davo::className(), ['id' => 'davo_id']);
    }
}
