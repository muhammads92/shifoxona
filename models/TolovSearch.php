<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;

/**
 * TolovSearch represents the model behind the search form of `app\models\Tolov`.
 */
class TolovSearch extends Tolov
{
    public $dateBegin;
    public $dateEnd;
    public $dateRange;
    public $dateBegin1;
    public $dateEnd1;
    public $dateRange1;
    public $date_davolanish;
    public $came_from;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'summa'], 'integer'],
            [['date', 'discription', 'dateBegin', 'dateEnd', 'dateRange', 'date_davolanish', 'dateBegin1', 'dateEnd1', 'dateRange1'], 'safe'],
            [['davolanish_id', 'came_from'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tolov::find();
        $query->joinWith('davolanish.bemor');

        // add conditions that should always apply here

        $sort = new Sort([
            'defaultOrder'=>['date'=>SORT_DESC, 'id'=>SORT_DESC],
            'attributes' => [
                'date',
                'summa',
                'discription',
                'davolanish_id',
                'id',
                'came_from',
                'date_davolanish' => [
                    'asc' => ['davolanish.date' => SORT_ASC, 'davolanish.id' => SORT_ASC],
                    'desc' => ['davolanish.date' => SORT_DESC, 'davolanish.id' => SORT_DESC],
//                    'default' => SORT_DESC,
//                    'label' => 'Name',
                ],
            ],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => $sort,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->dateBegin){
            $query->andWhere("tolov.date >='{$this->dateBegin}'");
        }
        if($this->dateEnd){
            $query->andWhere("tolov.date <='{$this->dateEnd}'");
        }
        if($this->dateBegin1){
            $query->andWhere("davolanish.date >='{$this->dateBegin1}'");
        }
        if($this->dateEnd1){
            $query->andWhere("davolanish.date <='{$this->dateEnd1}'");
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tolov.id' => $this->id,
            'tolov.summa' => $this->summa,
            'tolov.date' => $this->date,
            'davolanish.date' => $this->date_davolanish,
        ]);

//        $bemorr = $model->davolanish->bemor->fio;

        $query->andFilterWhere(['like', 'discription', $this->discription])
            ->andFilterWhere(['like', 'bemor.fio', $this->davolanish_id])
            ->andFilterWhere(['like', 'davolanish.came_from', $this->came_from]);

        return $dataProvider;
    }
}
