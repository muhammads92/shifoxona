<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bemor".
 *
 * @property int $id
 * @property string $fio
 * @property string $manzil
 * @property integer $active
 * @property string $descraption
 *
 * @property Davolanish[] $davolanishes
 */
class Bemor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bemor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'manzil'],   'required'],
            ['active', 'integer'],
            [['descraption'], 'string'],
            [['fio', 'manzil'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Ф.И.Ш.',
            'manzil' => 'Манзил',
            'descraption' => 'Маълумот',
            'active' => 'Бемор актив',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDavolanishes()
    {
        return $this->hasMany(Davolanish::className(), ['bemor_id' => 'id']);
    }
    public function getBatafsils()
    {
        return $this->hasMany(Batafsil::className(), ['bemor_id' => 'id']);
    }
    public static function all(){
        return \yii\helpers\ArrayHelper::map(self::find()->andFilterWhere(['active' => 1])->orderBy('fio')->all(), 'id', 'fio');
    }

    public function beforeDelete() {

        if (!parent::beforeDelete()) {
            return false;
        }

        foreach ($this->davolanishes as $davolanish){
            $davolanish->delete();
        }

        return true;
    }
}
