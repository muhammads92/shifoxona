<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Batafsil;

/**
 * BatafsilSearch represents the model behind the search form of `app\models\Batafsil`.
 */
class BatafsilSearch extends Batafsil
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'davolanish_id', 'narxi'], 'integer'],
            [['description'], 'safe'],
            [['davo_id'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Batafsil::find();
        $query->joinWith('davo');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'davolanish_id' => $this->davolanish_id,
            'narxi' => $this->narxi,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'davo.name', $this->davo_id]);

        return $dataProvider;
    }
}
