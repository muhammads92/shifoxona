<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Prixod;

/**
 * PrixodSearch represents the model behind the search form of `app\models\Prixod`.
 */
class PrixodSearch extends Prixod
{
    public $dateBegin;
    public $dateEnd;
    public $dateRange;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amound'], 'integer'],
            [['date', 'dori_id', 'dateBegin', 'dateEnd', 'dateRange'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prixod::find();
        $query -> joinWith('dori');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['date'=>SORT_DESC, 'id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->dateBegin){
            $query->andWhere("date >='{$this->dateBegin}'");
        }
        if($this->dateEnd){
            $query->andWhere("date <='{$this->dateEnd}'");
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'prixod.id' => $this->id,
            'prixod.amound' => $this->amound,
            'prixod.date' => $this->date,
        ]);
        $query->andFilterWhere(['like', 'dori.name', $this->dori_id]);

        return $dataProvider;
    }
}
