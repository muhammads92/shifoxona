<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Davolanish;

/**
 * DavolanishSearch represents the model behind the search form of `app\models\Davolanish`.
 */
class DavolanishSearch extends Davolanish
{
    public $dateRange;
    public $dateBegin;
    public $dateEnd;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            ['bemor_id', 'string'],
            [['date', 'came_from', 'dateBegin', 'dateEnd', 'dateRange'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Davolanish::find();
        $query->joinWith('bemor');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['date'=>SORT_DESC, 'id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->dateBegin){
            $query->andWhere("date >='{$this->dateBegin}'");
        }
        if($this->dateEnd){
            $query->andWhere("date <='{$this->dateEnd}'");
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'davolanish.id' => $this->id,
            'davolanish.date' => $this->date
        ]);

        $query->andFilterWhere(['like', 'came_from', $this->came_from])
            ->andFilterWhere(['like', 'bemor.fio', $this->bemor_id]);

        return $dataProvider;
    }
}
