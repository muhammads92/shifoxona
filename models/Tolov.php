<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tolov".
 *
 * @property int $id
 * @property int $davolanish_id
 * @property int $summa
 * @property string $date
 * @property string $discription
 *
 * @property Davolanish $davolanish
 */
class Tolov extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tolov';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['davolanish_id', 'summa', 'date', 'discription'], 'required'],
            [['davolanish_id', 'summa'], 'integer'],
            [['date', 'date_davolanish'], 'safe'],
            [['davolanish_id'], 'exist', 'skipOnError' => true, 'targetClass' => Davolanish::className(), 'targetAttribute' => ['davolanish_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'davolanish_id' => 'Даволаниш',
            'summa' => 'Сумма',
            'date' => 'Тўлов қилинган сана',
            'discription' => 'Маълумот',
            'date_davolanish' => 'Сана',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDavolanish()
    {
        return $this->hasOne(Davolanish::className(), ['id' => 'davolanish_id']);
    }
}
