<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rasxod".
 *
 * @property int $id
 * @property int $dori_id
 * @property int $amound
 * @property string $date
 *
 * @property Dori $d
 */
class Rasxod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rasxod';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dori_id', 'amound', 'date'], 'required'],
            [['dori_id', 'amound'], 'integer'],
            [['date'], 'safe'],
            [['dori_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dori::className(), 'targetAttribute' => ['dori_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dori_id' => 'Дори номи',
            'amound' => 'Сони',
            'date' => 'Сана',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDori()
    {
        return $this->hasOne(Dori::className(), ['id' => 'dori_id']);
    }
}
