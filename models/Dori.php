<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "dori".
 *
 * @property int $id
 * @property string $name
 * @property string $descraption
 *
 * @property Prixod[] $prixods
 * @property Rasxod[] $rasxods
 */
class Dori extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dori';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['descraption'], 'string'],
            [['name'], 'string', 'max' => 200],
            [['summa'], 'integer'],
//            ['balance', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Номи',
            'descraption' => 'Маълумот',
            'balance' => 'Қолдиқ',
            'summa' => 'Дори нархи',
//            'tsumma' => 'Тўлиқ сумма',
        ];
    }

    public function getBalance(){

//        $prixod = $this->getPrixods()->select(new Expression('sum(amound)'))->createCommand()->getRawSql();

        $prixod = $this->getPrixods()->select(new Expression('sum(amound)'))->scalar();
        $rasxod = $this->getRasxods()->select(new Expression('sum(amound)'))->scalar();
        return $prixod - $rasxod;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrixods()
    {
        return $this->hasMany(Prixod::className(), ['dori_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRasxods()
    {
        return $this->hasMany(Rasxod::className(), ['dori_id' => 'id']);
    }
    public static function all(){
        return \yii\helpers\ArrayHelper::map(self::find()->orderBy('name')->all(), 'id', 'name');
    }

    public function beforeDelete() {

        if (!parent::beforeDelete()) {
            return false;
        }

        foreach ($this->prixods as $prixod){
            $prixod->delete();
        }

        foreach ($this->rasxods as $rasxod){
            $rasxod->delete();
        }

        return true;
    }
}
