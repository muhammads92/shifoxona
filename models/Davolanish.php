<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "davolanish".
 *
 * @property int $id
 * @property string $date
 * @property int $bemor_id
 * @property string $came_from
 *
 * @property Batafsil[] $batafsils
 * @property Bemor $bemor
 * @property Tolov $tolovs
 */
class Davolanish extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'davolanish';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['date', 'bemor_id'], 'required'],
            [['date'], 'safe'],
            [['bemor_id'], 'integer'],
            [['came_from'], 'string', 'max' => 200],
            [['bemor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bemor::className(), 'targetAttribute' => ['bemor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'date' => 'Сана',
            'bemor_id' => 'Бемор Ф.И.Ш.',
            'came_from' => 'Ким орқали келган',
            'summa' => 'Сумма',
            'qarz' => 'Қарздорлик',
            'tolov' => 'Тўланган сумма',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatafsils() {
        return $this->hasMany(Batafsil::className(), ['davolanish_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBemor() {
        return $this->hasOne(Bemor::className(), ['id' => 'bemor_id']);
    }

    public function getTolovs() {
        return $this->hasMany(Tolov::className(), ['davolanish_id' => 'id']);
    }

    public static function all() {

        $list = self::find()
                        ->select([
                            'davolanish.id',
                            new Expression('concat_ws(" / ", DATE_FORMAT(davolanish.date, "%d.%m.%Y"), bemor.fio, davolanish.came_from) as name'),
                        ])
                        ->joinWith('bemor')
                        ->orderBy(['davolanish.date' => SORT_DESC, 'bemor.fio' => SORT_ASC])
                        ->asArray()
                        ->createCommand()->queryAll();

        return \yii\helpers\ArrayHelper::map($list, 'id', 'name');
    }

    public function getSumma() {
        $summa = $this->getBatafsils()->select(new Expression('sum(narxi)'))->scalar();

        if ($summa === null) {
            $summa = 0;
        }
        return $summa;
    }

    public function getQarz() {
        $summa = $this->getBatafsils()->select(new Expression('sum(narxi)'))->scalar();
        $tolov = $this->getTolovs()->select(new Expression('sum(summa)'))->scalar();
        $qarz = $summa - $tolov;
//        if ($qarz > 0){
//            $qarz = 0;
//        }
        return $qarz;
    }

    public function getTolov() {
        $tolov = $this->getTolovs()->select(new Expression('sum(summa)'))->scalar();
        if ($tolov === null) {
            $tolov = 0;
        }
        return $tolov;
    }

    public function beforeDelete() {
        
        if (!parent::beforeDelete()) {
            return false;
        }
        
        foreach ($this->batafsils as $batafsil){
            $batafsil->delete();
        }
        
        foreach ($this->tolovs as $tolov){
            $tolov->delete();
        }

        return true;
    }

}
