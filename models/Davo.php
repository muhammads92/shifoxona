<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "davo".
 *
 * @property int $id
 * @property string $name
 * @property int $summa
 * @property string $descraption
 *
 * @property Batafsil[] $batafsils
 */
class Davo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'davo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'summa'], 'required'],
            [['summa'], 'integer'],
            [['descraption'], 'string'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Номи',
            'summa' => 'Нархи',
            'descraption' => 'Маълумот',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatafsils()
    {
        return $this->hasMany(Batafsil::className(), ['davo_id' => 'id']);
    }

    public static function all(){
        return \yii\helpers\ArrayHelper::map(self::find()->orderBy('name')->all(), 'id', 'name');
    }
}
