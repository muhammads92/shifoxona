<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DavolanishNewAsset extends AssetBundle
{
    public $sourcePath = '@app/themes/jeasyui/views/davolanish/assets';
    public $css = [
        'css/davolanish-new.css',
    ];
    public $js = [
        'js/davolanish-new.js'
    ];
    public $depends = [];
    public $publishOptions=['forceCopy'=>YII_DEBUG];
}
