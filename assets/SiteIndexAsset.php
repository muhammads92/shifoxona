<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SiteIndexAsset extends AssetBundle
{
    public $sourcePath = '@app/themes/jeasyui/views/jeasyui/index/assets';
    public $css = [
        'css/site-index.css',
    ];
    public $js = [
        'js/site-index.js'
    ];
    public $depends = [];
    public $publishOptions=['forceCopy'=>YII_DEBUG];
}
