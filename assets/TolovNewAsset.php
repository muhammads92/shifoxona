<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TolovNewAsset extends AssetBundle
{
    public $sourcePath = '@app/themes/jeasyui/views/tolov/assets';
    public $css = [
        'css/tolov-new.css',
    ];
    public $js = [
        'js/tolov-new.js'
    ];
    public $depends = [];
    public $publishOptions=['forceCopy'=>YII_DEBUG];
}
