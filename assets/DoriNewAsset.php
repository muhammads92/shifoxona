<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DoriNewAsset extends AssetBundle
{
    public $sourcePath = '@app/views/dori/assets';
    public $css = [
        'css/dori-new.css',
    ];
    public $js = [
        'js/dori-new.js'
    ];
    public $depends = [];
    public $publishOptions=['forceCopy'=>YII_DEBUG];
}
