-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 01 2018 г., 20:10
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shifoxona`
--

-- --------------------------------------------------------

--
-- Структура таблицы `batafsil`
--

CREATE TABLE `batafsil` (
  `id` int(11) NOT NULL,
  `davolanish_id` int(11) NOT NULL,
  `davo_id` int(11) NOT NULL,
  `narxi` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `batafsil`
--

INSERT INTO `batafsil` (`id`, `davolanish_id`, `davo_id`, `narxi`, `description`) VALUES
(39, 4, 7, 520, 'ljhg'),
(40, 25, 5, 2000, 'gfgfv'),
(42, 27, 2, 10250, 'c,jksdbcjkd'),
(43, 20, 2, 120000, 'ldtyrghhj'),
(44, 20, 3, 520, 'klergn'),
(47, 27, 1, 410252, 'fkdnbl'),
(48, 27, 7, 4000000, 'eojfgodfgl');

-- --------------------------------------------------------

--
-- Структура таблицы `bemor`
--

CREATE TABLE `bemor` (
  `id` int(11) NOT NULL,
  `fio` varchar(200) NOT NULL,
  `manzil` varchar(200) NOT NULL,
  `descraption` text NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `bemor`
--

INSERT INTO `bemor` (`id`, `fio`, `manzil`, `descraption`, `active`) VALUES
(1, 'Toxirov Javohir', 'Yunusobod Tum.', 'TATU talabasi.', 1),
(2, 'Qodirov Akbar', 'Chilonzor Tum.', 'TATU talabasi.', 1),
(4, 'Rayimov Mirjalol', 'M.Gorkiy', 'Qisqacha ma\'lumot.', 1),
(6, 'Sulaymonov Zohid', 'Samarqand', 'ofihduifhefuiohfh', 1),
(7, 'Ubaydullayev Fazliddin', 'Toshkent vil.', ';rhfgildh;', 1),
(8, 'phmrtpo', 'poppgoneponhf', 'ponponpnotasd', 1),
(9, 'Abduraxmanov Bobur', 'Chilonzor', 'fgbdfbv', 1),
(10, 'Ramazonov Behruz', 'Xamza', 'kjnfohkl', 1),
(12, 'asd', 'asd', 'shcsdfnlsich', 0),
(14, 'kim', 'kim', 'kim', 0),
(15, 'kim', 'kim', 'kim', 1),
(16, 'kimxkgws', 'glxfh', 'gkh', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `davo`
--

CREATE TABLE `davo` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `summa` int(11) NOT NULL,
  `descraption` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `davo`
--

INSERT INTO `davo` (`id`, `name`, `summa`, `descraption`) VALUES
(1, 'Gripp', 40000, 'Grippni davolash uchun.'),
(2, 'Shamollash', 120000, 'Shamollashni davolash uchun.'),
(3, 'Bronxit', 200000, 'fefwrfohifoihfohikfnsdv'),
(5, 'Tumov', 100000, 'gkofjgorjg'),
(7, 'Tish og\'rig\'i', 250000, 'frefergergf'),
(8, 'liguhk;j', 6320, 'fdvkjhoi');

-- --------------------------------------------------------

--
-- Структура таблицы `davolanish`
--

CREATE TABLE `davolanish` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `bemor_id` int(11) NOT NULL,
  `came_from` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `davolanish`
--

INSERT INTO `davolanish` (`id`, `date`, `bemor_id`, `came_from`) VALUES
(4, '2018-03-21', 4, 'ujyhtgefcd'),
(20, '2018-03-31', 2, 'asdasd'),
(22, '2018-03-15', 2, '2oefpqwkqpd'),
(24, '2018-03-21', 7, 'tfuhjkjgckihdc'),
(25, '2018-03-12', 10, 'lkmjkn'),
(27, '2018-03-23', 9, 'jlvhkcgjbn'),
(28, '2018-03-14', 2, 'fvdfv fds'),
(29, '2018-03-14', 10, 'iufhcd'),
(30, '2018-03-14', 10, 'vkldrfnbv'),
(31, '2018-03-20', 1, 'efdc'),
(33, '2018-03-16', 6, 'kfufjhkgjl');

-- --------------------------------------------------------

--
-- Структура таблицы `dori`
--

CREATE TABLE `dori` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `descraption` text NOT NULL,
  `summa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dori`
--

INSERT INTO `dori` (`id`, `name`, `descraption`, `summa`) VALUES
(1, 'Aspirin', 'Shamollash uchun.', 1500),
(3, 'Taylol Xot', 'Gripp uchun.', 3500),
(4, 'Glyukoza', 'Ukol.jkgb', 6500),
(8, 'Analgin', 'Tabletka', 1200),
(9, 'Dimedirol', 'Ukol', 3600),
(10, 'Askorbinka', 'Tabletka', 2300);

-- --------------------------------------------------------

--
-- Структура таблицы `prixod`
--

CREATE TABLE `prixod` (
  `id` int(11) NOT NULL,
  `dori_id` int(11) NOT NULL,
  `amound` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prixod`
--

INSERT INTO `prixod` (`id`, `dori_id`, `amound`, `date`) VALUES
(6, 1, 10, '2018-02-20'),
(8, 4, 5, '2018-02-20'),
(9, 4, 2, '2018-02-22'),
(13, 1, 4, '2018-02-22'),
(16, 3, 10, '2018-02-09');

-- --------------------------------------------------------

--
-- Структура таблицы `rasxod`
--

CREATE TABLE `rasxod` (
  `id` int(11) NOT NULL,
  `dori_id` int(11) NOT NULL,
  `amound` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `rasxod`
--

INSERT INTO `rasxod` (`id`, `dori_id`, `amound`, `date`) VALUES
(1, 1, 1, '2018-02-20'),
(2, 4, 2, '2004-02-20'),
(3, 4, 2, '2005-02-20'),
(9, 3, 4, '2018-02-27'),
(10, 1, 10, '2018-03-07');

-- --------------------------------------------------------

--
-- Структура таблицы `tolov`
--

CREATE TABLE `tolov` (
  `id` int(11) NOT NULL,
  `davolanish_id` int(11) NOT NULL,
  `summa` int(11) NOT NULL,
  `date` date NOT NULL,
  `discription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tolov`
--

INSERT INTO `tolov` (`id`, `davolanish_id`, `summa`, `date`, `discription`) VALUES
(8, 20, 1200020, '2018-01-03', 'ljkvhcgjvhbk'),
(14, 25, 21000, '2018-03-23', 'rfgvfv'),
(15, 27, 120000, '2018-03-01', 'ljkhcgjvhb'),
(16, 4, 12000, '2018-03-09', 'jedhfjkhdfj'),
(19, 27, 20205, '2018-03-06', 'ojihugyf'),
(20, 20, 52000000, '2018-03-14', 'efnk');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `batafsil`
--
ALTER TABLE `batafsil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `davolanish_id` (`davolanish_id`),
  ADD KEY `davo_id` (`davo_id`);

--
-- Индексы таблицы `bemor`
--
ALTER TABLE `bemor`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `davo`
--
ALTER TABLE `davo`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `davolanish`
--
ALTER TABLE `davolanish`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bemor_id` (`bemor_id`);

--
-- Индексы таблицы `dori`
--
ALTER TABLE `dori`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prixod`
--
ALTER TABLE `prixod`
  ADD PRIMARY KEY (`id`),
  ADD KEY `d_id` (`dori_id`);

--
-- Индексы таблицы `rasxod`
--
ALTER TABLE `rasxod`
  ADD PRIMARY KEY (`id`),
  ADD KEY `d_id` (`dori_id`);

--
-- Индексы таблицы `tolov`
--
ALTER TABLE `tolov`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tolov_kk` (`davolanish_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `batafsil`
--
ALTER TABLE `batafsil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT для таблицы `bemor`
--
ALTER TABLE `bemor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `davo`
--
ALTER TABLE `davo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `davolanish`
--
ALTER TABLE `davolanish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблицы `dori`
--
ALTER TABLE `dori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `prixod`
--
ALTER TABLE `prixod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `rasxod`
--
ALTER TABLE `rasxod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `tolov`
--
ALTER TABLE `tolov`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `batafsil`
--
ALTER TABLE `batafsil`
  ADD CONSTRAINT `batafsil_ibfk_1` FOREIGN KEY (`davolanish_id`) REFERENCES `davolanish` (`id`),
  ADD CONSTRAINT `batafsil_ibfk_2` FOREIGN KEY (`davo_id`) REFERENCES `davo` (`id`);

--
-- Ограничения внешнего ключа таблицы `davolanish`
--
ALTER TABLE `davolanish`
  ADD CONSTRAINT `davolanish_ibfk_1` FOREIGN KEY (`bemor_id`) REFERENCES `bemor` (`id`);

--
-- Ограничения внешнего ключа таблицы `prixod`
--
ALTER TABLE `prixod`
  ADD CONSTRAINT `prixod_ibfk_1` FOREIGN KEY (`dori_id`) REFERENCES `dori` (`id`);

--
-- Ограничения внешнего ключа таблицы `rasxod`
--
ALTER TABLE `rasxod`
  ADD CONSTRAINT `rasxod_ibfk_1` FOREIGN KEY (`dori_id`) REFERENCES `dori` (`id`);

--
-- Ограничения внешнего ключа таблицы `tolov`
--
ALTER TABLE `tolov`
  ADD CONSTRAINT `tolov_kk` FOREIGN KEY (`davolanish_id`) REFERENCES `davolanish` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
