<?php

namespace app\controllers;

use app\models\Prixod;
use app\models\Rasxod;
use Yii;
use app\models\Dori;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * DoriController implements the CRUD actions for Dori model.
 */
class DoriController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Workorder models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->request->isAjax){
            echo $this->renderAjax('_index');
            Yii::$app->end();
        }else{
            return $this->render('index');
        }
    }
 
    public function actionGetListData($page = 1, $rows = 10) {
        if (Yii::$app->request->isAjax) {
            $result = [];
            $query = Dori::find();
            $query->select('COUNT(1)');
            if(isset($_GET['filterRules'])){
                $filters = Json::decode($_GET['filterRules']);
                foreach ($filters as $filter){
                    $query->andWhere(['like', $filter['field'], $filter['value']]);
                }
            }

            $result['total'] = $query->scalar();
            
            $query->select('*')->orderBy(['name' => SORT_ASC]);
            $query->offset(($page * $rows) - $rows);
            $query->limit($rows);
            $result['rows'] = $query->asArray()->all();
            foreach ($result['rows'] as &$row) {
                $row['balance'] = Dori::findOne($row['id'])->balance;
            }
            echo Json::encode($result);
            Yii::$app->end();
        }
    }

    public function actionGetPrixodListData($page = 1, $rows = 10, $dori_id) {
        if (Yii::$app->request->isAjax) {
            $result = [];
            $query = Prixod::find();
            $query->where(['dori_id'=>$dori_id]);
            $query->select('COUNT(1)');
            if(isset($_GET['filterRules'])){
                $filters = Json::decode($_GET['filterRules']);
                foreach ($filters as $filter){
                        $query->andWhere(['like', $filter['field'], $filter['value']]);
                }
            }
            $result['total'] = $query->scalar();
            $query->select('*')->orderBy(['date' => SORT_DESC]);
            $query->offset(($page * $rows) - $rows);
            $query->limit($rows);
            $result['rows'] = $query->asArray()->all();
            echo Json::encode($result);
            Yii::$app->end();
        }
    }

    public function actionGetRasxodListData($page = 1, $rows = 10, $dori_id) {
        if (Yii::$app->request->isAjax) {
            $result = [];
            $query = Rasxod::find();
            $query->where(['dori_id'=>$dori_id]);
            $query->select('COUNT(1)');
            if(isset($_GET['filterRules'])){
                $filters = Json::decode($_GET['filterRules']);
                foreach ($filters as $filter){
                        $query->andWhere(['like', $filter['field'], $filter['value']]);
                }
            }
            $result['total'] = $query->scalar();
            $query->select('*')->orderBy(['date' => SORT_DESC]);
            $query->offset(($page * $rows) - $rows);
            $query->limit($rows);
            $result['rows'] = $query->All();
            echo Json::encode($result);
            Yii::$app->end();
        }
    }

    public function actionSave(){

        $model = new Dori();
        $model->load($_POST, '');

        if($model->validate()){
            $saved = $model->save();
        }else{
            $saved = false;
        }

        if($saved){
            $model = Dori::findOne($model->id);

            $res = $model->getAttributes();
        }else{
            $res = [];
            $res['isError'] = true;
            $res['errors'] = $model->errors;
        }

        echo Json::encode($res);
        Yii::$app->end();
    }

    public function actionUpdate(){

        if(isset($_POST['id']) && ctype_digit($_POST['id'])){

            $model = Dori::findOne($_POST['id']);
            $model->load($_POST, '');

            if ($model->validate()) {
                $saved = $model->save();
            } else {
                $saved = false;
            }

            if ($saved) {
                $model = Dori::findOne($model->id);

                $res = $model->getAttributes();
            } else {
                $res = [];
                $res['isError'] = true;
                $res['errors'] = $model->errors;
            }

            echo Json::encode($res);
            Yii::$app->end();
        }
    }

    public function actionDestroy(){

        if(isset($_POST['id']) && ctype_digit($_POST['id'])){

            $model = Dori::findOne($_POST['id']);
            $deleted = $model->delete();

            if ($deleted) {
                $model = Dori::findOne($model->id);

                $res = ['success'=>true];
            } else {
                $res = [];
                $res['isError'] = true;
                $res['errors'] = $model->errors;
            }

            echo Json::encode($res);
            Yii::$app->end();
        }
    }

    public function actionPsave(){

        $_POST['date'] = preg_replace('/^(\d{2})\/(\d{2})\/(\d{4})$/', '$3-$1-$2', $_POST['date']);
        $model = new Prixod();
        $model->load($_POST, '');

        if($model->validate()){
            $saved = $model->save();
        }else{
            $saved = false;
        }

        if($saved){
            $model = Prixod::findOne($model->id);

            $res = $model->getAttributes();
        }else{
            $res = [];
            $res['isError'] = true;
            $res['errors'] = $model->errors;
        }

        echo Json::encode($res);
        Yii::$app->end();
    }

    public function actionPupdate(){

        $_POST['date'] = preg_replace('/^(\d{2})\/(\d{2})\/(\d{4})$/', '$3-$1-$2', $_POST['date']);
        if(isset($_POST['id']) && ctype_digit($_POST['id'])){

            $model = Prixod::findOne($_POST['id']);
            $model->load($_POST, '');

            if ($model->validate()) {
                $saved = $model->save();
            } else {
                $saved = false;
            }

            if ($saved) {
                $model = Prixod::findOne($model->id);

                $res = $model->getAttributes();
            } else {
                $res = [];
                $res['isError'] = true;
                $res['errors'] = $model->errors;
            }

            echo Json::encode($res);
            Yii::$app->end();
        }
    }

    public function actionPdestroy(){

        if(isset($_POST['id']) && ctype_digit($_POST['id'])){

            $model = Prixod::findOne($_POST['id']);
            $deleted = $model->delete();

            if ($deleted) {
                $model = Prixod::findOne($model->id);

                $res = ['success'=>true];
            } else {
                $res = [];
                $res['isError'] = true;
                $res['errors'] = $model->errors;
            }

            echo Json::encode($res);
            Yii::$app->end();
        }
    }

    public function actionRsave(){

        $_POST['date'] = preg_replace('/^(\d{2})\/(\d{2})\/(\d{4})$/', '$3-$1-$2', $_POST['date']);
        $model = new Rasxod();
        $model->load($_POST, '');

        if($model->validate()){
            $saved = $model->save();
        }else{
            $saved = false;
        }

        if($saved){
            $model = Rasxod::findOne($model->id);

            $res = $model->getAttributes();
        }else{
            $res = [];
            $res['isError'] = true;
            $res['errors'] = $model->errors;
        }

        echo Json::encode($res);
        Yii::$app->end();
    }

    public function actionRupdate(){

        $_POST['date'] = preg_replace('/^(\d{2})\/(\d{2})\/(\d{4})$/', '$3-$1-$2', $_POST['date']);
        if(isset($_POST['id']) && ctype_digit($_POST['id'])){

            $model = Rasxod::findOne($_POST['id']);
            $model->load($_POST, '');

            if ($model->validate()) {
                $saved = $model->save();
            } else {
                $saved = false;
            }

            if ($saved) {
                $model = Rasxod::findOne($model->id);

                $res = $model->getAttributes();
            } else {
                $res = [];
                $res['isError'] = true;
                $res['errors'] = $model->errors;
            }

            echo Json::encode($res);
            Yii::$app->end();
        }
    }

    public function actionRdestroy(){

        if(isset($_POST['id']) && ctype_digit($_POST['id'])){

            $model = Rasxod::findOne($_POST['id']);
            $deleted = $model->delete();

            if ($deleted) {
                $model = Rasxod::findOne($model->id);

                $res = ['success'=>true];
            } else {
                $res = [];
                $res['isError'] = true;
                $res['errors'] = $model->errors;
            }

            echo Json::encode($res);
            Yii::$app->end();
        }
    }

    public function actionNew() {
        $model = new Dori();

        if ($model->load(Yii::$app->request->post())) {
            $result=[];
            if ($model->save(true)) {
                $result['status'] = 'success';
            } else {
                $result['status'] = 'error';
                $result['error'] =  $model->getErrors();
            }
            echo Json::encode($result);
        } else {
            if (Yii::$app->request->isAjax) {
                echo $this->renderAjax('_new', ['model' => $model]);
                Yii::$app->end();
            } else {
                return $this->render('new');
            }
        }
    }
    
    /**
     * Deletes an existing Dori model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
                $arrPost = Yii::$app->request->post('ids');
        foreach($arrPost as $ids){
            $this->findModel($ids["id"])->delete();
        }

        echo Json::encode(['status' => 'success']);
        Yii::$app->end();
    }
  
    /**
     * Finds the Dori model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dori the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dori::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
