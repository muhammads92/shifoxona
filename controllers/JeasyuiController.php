<?php

namespace app\controllers;

use Yii;
//use yii\web\Controller;
//use yii\filters\VerbFilter;
//use yii\filters\AccessControl;
use yii\helpers\Json;
use app\models\LoginForm;

/**
 * Site controller
 */
class JeasyuiController extends \sheillendra\jeasyui\controllers\JeasyuiController {
    

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        $req = Yii::$app->getRequest();
        if (!Yii::$app->user->isGuest) {
            if ($req->isAjax) {
                echo Json::encode(['redirect' => Yii::$app->getHomeUrl()]);
                return Yii::$app->end();
            }else{
                return $this->redirect(['/']);
            }
        }

        $model = new LoginForm();
        if ($model->load($req->post()) && $model->login()) {
            echo Json::encode([
                'redirect' => Yii::$app->getUser()->getReturnUrl(),
                'data' => [
                    'token' => $model->getUser()->getToken(),
                ]
            ]);
        } else {
            if ($model->hasErrors()) {
                echo Json::encode(['loginerror' => $model->getErrors()]);
            } else {
                return $this->render('login/login', ['model' => $model]);
            }
        }
        return Yii::$app->end();
    }
}
