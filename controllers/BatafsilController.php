<?php

namespace app\controllers;

use Yii;
use app\models\Batafsil;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * BatafsilController implements the CRUD actions for Batafsil model.
 */
class BatafsilController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Workorder models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->request->isAjax){
            echo $this->renderAjax('_index');
            Yii::$app->end();
        }else{
            return $this->render('index');
        }
    }
 
    public function actionGetListData($page = 1, $rows = 10) {
        if (Yii::$app->request->isAjax) {
            $result = [];
            $query = Batafsil::find();
            $query->select('COUNT(1)');
            $result['total'] = $query->scalar();
            
            $query->select('*');
            $query->offset(($page * $rows) - $rows);
            $query->limit($rows);
            $result['rows'] = $query->All();
            echo Json::encode($result);
            Yii::$app->end();
        }
    }

    public function actionNew() {
        $model = new Batafsil();

        if ($model->load(Yii::$app->request->post())) {
            $result=[];
            if ($model->save(true)) {
                $result['status'] = 'success';
            } else {
                $result['status'] = 'error';
                $result['error'] =  $model->getErrors();
            }
            echo Json::encode($result);
        } else {
            if (Yii::$app->request->isAjax) {
                echo $this->renderAjax('_new', ['model' => $model]);
                Yii::$app->end();
            } else {
                return $this->render('new');
            }
        }
    }
    
    /**
     * Deletes an existing Batafsil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
                $arrPost = Yii::$app->request->post('ids');
        foreach($arrPost as $ids){
            $this->findModel($ids["id"])->delete();
        }

        echo Json::encode(['status' => 'success']);
        Yii::$app->end();
    }
  
    /**
     * Finds the Batafsil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Batafsil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Batafsil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
