<?php
namespace app\controllers;


use app\models\Davo;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * CourierController implements the CRUD actions for Courier model.
 */
class ReportController extends Controller
{

    public function actionR11($id){
        $model = \app\models\Davolanish::findOne($id);

        return $this->renderPartial('r11', [
            'model' => $model
        ]);
    }
}
?>