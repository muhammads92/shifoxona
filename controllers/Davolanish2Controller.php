<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Davolanish;
use app\models\DavolanishSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Batafsil;
use app\models\Model;
use yii\helpers\ArrayHelper;

/**
 * DavolanishController implements the CRUD actions for Davolanish model.
 */
class DavolanishController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['logout'],
                'rules' => [
                    [
//                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Davolanish models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DavolanishSearch();

        $params = Yii::$app->request->queryParams;
        if(isset($params['DavolanishSearch']['dateRange']) && $params['DavolanishSearch']['dateRange']){
            $params['DavolanishSearch']['dateBegin'] = preg_replace('/(\d{2}).(\d{2}).(\d{1,4}) - (\d{2}).(\d{2}).(\d{1,4})/', '$3-$2-$1', $params['DavolanishSearch']['dateRange']);
            $params['DavolanishSearch']['dateEnd'] = preg_replace('/(\d{2}).(\d{2}).(\d{1,4}) - (\d{2}).(\d{2}).(\d{1,4})/', '$6-$5-$4', $params['DavolanishSearch']['dateRange']);
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    /**
     * Displays a single Davolanish model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Davolanish model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Davolanish();
        $modelsBatafsil = [new Batafsil()];
        $flag = $model->load(Yii::$app->request->post());
        $model->date = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/', '$3-$2-$1', $model->date);

        if ($flag) {

            $modelsBatafsil = Model::createMultiple(Batafsil::classname());
            Model::loadMultiple($modelsBatafsil, Yii::$app->request->post());


            // validate all models
            $valid = $model->validate();

            $modelBatafsilAttributesForValidate = (new Batafsil())->attributes();
            unset($modelBatafsilAttributesForValidate['davolanish_id']);

            $valid = Model::validateMultiple($modelsBatafsil, $modelBatafsilAttributesForValidate) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save()) {

                        foreach ($modelsBatafsil as $modelBatafsil) {
                            $modelBatafsil->davolanish_id = $model->id;
                            if (! ($flag = $modelBatafsil->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelsBatafsil' => (empty($modelsBatafsil)) ? [new Batafsil()] : $modelsBatafsil,
        ]);
    }

    /**
     * Updates an existing Davolanish model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsBatafsil = $model->batafsils;
        $flag = $model->load(Yii::$app->request->post());
        $model->date = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/', '$3-$2-$1', $model->date);

        if ($flag) {

            $modelsBatafsil = Model::createMultiple(Batafsil::classname(), $modelsBatafsil);

            Model::loadMultiple($modelsBatafsil, Yii::$app->request->post());

            // validate all models

            $oldIDs = ArrayHelper::map($modelsBatafsil, 'id', 'id');
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsBatafsil) && $valid;
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsBatafsil, 'id', 'id')));

            if ($valid) {

                $transaction = \Yii::$app->db->beginTransaction();

                try {

                    if ($flag = $model->save(false)) {

                        if (!empty($deletedIDs)) {

                            Batafsil::deleteAll(['id' => $deletedIDs]);

                        }

                        foreach ($modelsBatafsil as $modelBatafsil) {

                            $modelBatafsil->davolanish_id = $model->id;

                            if (!($flag = $modelBatafsil->save(false))) {

                                $transaction->rollBack();

                                break;
                            }
                        }
                    }

                    if ($flag) {

                        $transaction->commit();

                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {

                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [

            'model' => $model,

            'modelsBatafsil' => (empty($modelsBatafsil)) ? [new Batafsil()] : $modelsBatafsil

        ]);

    }

    /**
     * Deletes an existing Davolanish model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        foreach ($model->batafsils as $batafsil) {
            $batafsil->delete();
        }
        foreach ($model->tolovs as $tolov) {
            $tolov->delete();
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Davolanish model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Davolanish the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Davolanish::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
