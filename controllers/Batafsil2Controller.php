<?php

namespace app\controllers;

use app\models\Davolanish;
use yii\filters\AccessControl;
use Yii;
use app\models\Batafsil;
use app\models\BatafsilSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BatafsilController implements the CRUD actions for Batafsil model.
 */
class BatafsilController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['logout'],
                'rules' => [
                    [
//                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Batafsil models.
     * @return mixed
     */
    public function actionIndex($davolanish_id)
    {
        $searchModel = new BatafsilSearch();

        $params = Yii::$app->request->queryParams;
        $params['BatafsilSearch']['davolanish_id'] = $davolanish_id;
        $dataProvider = $searchModel->search($params);

        $davolanish = Davolanish::findOne($davolanish_id);
        //$davolanish = Davolanish::find()->where(['id'=>$davolanish_id])->one();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'davolanish' => $davolanish,
        ]);
    }

    /**
     * Displays a single Batafsil model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Batafsil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($davolanish_id)
    {
        $model = new Batafsil();
        $davolanish = Davolanish::findOne($davolanish_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'davolanish' => $davolanish,
        ]);
    }

    /**
     * Updates an existing Batafsil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $davolanish = Davolanish::findOne($davolanish_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Batafsil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $davolanish = $model->davolanish_id;

        $this->findModel($id)->delete();

        return $this->redirect(['index', 'davolanish_id'=>$davolanish]);
    }

    /**
     * Finds the Batafsil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Batafsil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Batafsil::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
