<?php

namespace app\controllers;

use Yii;
use app\models\Tolov;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * TolovController implements the CRUD actions for Tolov model.
 */
class TolovController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Workorder models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->request->isAjax){
            echo $this->renderAjax('_index');
            Yii::$app->end();
        }else{
            return $this->render('index');
        }
    }
 
    public function actionGetListData($page = 1, $rows = 10) {
        if (Yii::$app->request->isAjax) {
            $result = [];
            $query = Tolov::find();
            $query->select('COUNT(1)');
            if(isset($_GET['filterRules'])){
                $filters = Json::decode($_GET['filterRules']);
                foreach ($filters as $filter){
                    $query->andWhere(['like', $filter['field'], $filter['value']]);
                }
            }
            $result['total'] = $query->scalar();
            
            $query->select('*');
            $query->offset(($page * $rows) - $rows);
            $query->limit($rows);
            $result['rows'] = $query->All();
            echo Json::encode($result);
            Yii::$app->end();
        }
    }

    public function actionNew() {
        $model = new Tolov();

        $flag = $model->load(Yii::$app->request->post());
        $model->date = preg_replace('/^(\d{2})\/(\d{2})\/(\d{4})$/', '$3-$1-$2', $model->date);
        if ($flag) {
            $result=[];
            if ($model->save(true)) {
                $result['status'] = 'success';
            } else {
                $result['status'] = 'error';
                $result['error'] =  $model->getErrors();
            }
            echo Json::encode($result);
        } else {
            if (Yii::$app->request->isAjax) {
                echo $this->renderAjax('_new', ['model' => $model]);
                Yii::$app->end();
            } else {
                return $this->render('new');
            }
        }
    }

    public function actionSave(){

        $_POST['date'] = preg_replace('/^(\d{2})\/(\d{2})\/(\d{4})$/', '$3-$1-$2', $_POST['date']);
        $model = new Tolov();
        $model->load($_POST, '');

        if($model->validate()){
//            print_r($_POST);
//            die();
            $saved = $model->save();
        }else{
            $saved = false;
        }

        if($saved){
            $model = Tolov::findOne($model->id);

            $res = $model->getAttributes();
        }else{
            $res = [];
            $res['isError'] = true;
            $res['errors'] = $model->errors;
        }

        echo Json::encode($res);
        Yii::$app->end();
    }

    public function actionUpdate(){

        $_POST['date'] = preg_replace('/^(\d{2})\/(\d{2})\/(\d{4})$/', '$3-$1-$2', $_POST['date']);
        if(isset($_POST['id']) && ctype_digit($_POST['id'])){

            $model = Tolov::findOne($_POST['id']);
            $model->load($_POST, '');

            if ($model->validate()) {
                $saved = $model->save();
            } else {
                $saved = false;
            }

            if ($saved) {
                $model = Tolov::findOne($model->id);

                $res = $model->getAttributes();
            } else {
                $res = [];
                $res['isError'] = true;
                $res['errors'] = $model->errors;
            }

            echo Json::encode($res);
            Yii::$app->end();
        }
    }

    public function actionDestroy(){

        if(isset($_POST['id']) && ctype_digit($_POST['id'])){

            $model = Tolov::findOne($_POST['id']);
            $deleted = $model->delete();

            if ($deleted) {
                $model = Tolov::findOne($model->id);

                $res = ['success'=>true];
            } else {
                $res = [];
                $res['isError'] = true;
                $res['errors'] = $model->errors;
            }

            echo Json::encode($res);
            Yii::$app->end();
        }
    }
    
    /**
     * Deletes an existing Tolov model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
                $arrPost = Yii::$app->request->post('ids');
        foreach($arrPost as $ids){
            $this->findModel($ids["id"])->delete();
        }

        echo Json::encode(['status' => 'success']);
        Yii::$app->end();
    }
  
    /**
     * Finds the Tolov model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tolov the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tolov::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
