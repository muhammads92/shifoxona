<?php

namespace app\controllers;

use Yii;
use app\models\Bemor;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * BemorController implements the CRUD actions for Bemor model.
 */
class BemorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Workorder models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->request->isAjax){
            echo $this->renderAjax('_index');
            Yii::$app->end();
        }else{
            return $this->render('index');
        }
    }
 
    public function actionGetListData($page = 1, $rows = 10) {
        if (Yii::$app->request->isAjax) {
            $result = [];
            $query = Bemor::find()->orderBy(['active'=>SORT_DESC, 'fio'=>SORT_ASC,]);
            $query->select('COUNT(1)');
            if(isset($_GET['filterRules'])){
                $filters = Json::decode($_GET['filterRules']);
                foreach ($filters as $filter){
                    $query->andWhere(['like', $filter['field'], $filter['value']]);
                }
            }
            $result['total'] = $query->scalar();
            
            $query->select('*');
            $query->offset(($page * $rows) - $rows);
            $query->limit($rows);
            $result['rows'] = $query->All();
            echo Json::encode($result);
            Yii::$app->end();
        }
    }

    public function actionGetDropdownList() {
//        if (Yii::$app->request->isAjax) {
            $res = Bemor::find()
                ->select(['id', 'fio'])
                ->andFilterWhere(['active' => 1])
                ->orderBy(['fio'=>SORT_ASC])->asArray()->all();
            echo Json::encode($res);
            Yii::$app->end();
//        }
    }

    public function actionNew() {
        $model = new Bemor();

        if ($model->load(Yii::$app->request->post())) {
            $result=[];
            if ($model->save(true)) {
                $result['status'] = 'success';
            } else {
                $result['status'] = 'error';
                $result['error'] =  $model->getErrors();
            }
            echo Json::encode($result);
        } else {
            if (Yii::$app->request->isAjax) {
                echo $this->renderAjax('_new', ['model' => $model]);
                Yii::$app->end();
            } else {
                return $this->render('new');
            }
        }
    }

    public function actionSave(){

        $model = new Bemor();
        $model->load($_POST, '');

        if($model->validate()){
//            print_r($_POST);
//            die();
            $saved = $model->save();
        }else{
            $saved = false;
        }

        if($saved){
            $model = Bemor::findOne($model->id);

            $res = $model->getAttributes();
        }else{
            $res = [];
            $res['isError'] = true;
            $res['errors'] = $model->errors;
        }

        echo Json::encode($res);
        Yii::$app->end();
    }

    public function actionUpdate(){

        if(isset($_POST['id']) && ctype_digit($_POST['id'])){

            $model = Bemor::findOne($_POST['id']);
            $model->load($_POST, '');

            if ($model->validate()) {
                $saved = $model->save();
            } else {
                $saved = false;
            }

            if ($saved) {
                $model = Bemor::findOne($model->id);

                $res = $model->getAttributes();
            } else {
                $res = [];
                $res['isError'] = true;
                $res['errors'] = $model->errors;
            }

            echo Json::encode($res);
            Yii::$app->end();
        }
    }

    public function actionDestroy(){

        if(isset($_POST['id']) && ctype_digit($_POST['id'])){

            $model = Bemor::findOne($_POST['id']);
            $deleted = $model->delete();

            if ($deleted) {
                $model = Bemor::findOne($model->id);

                $res = ['success'=>true];
            } else {
                $res = [];
                $res['isError'] = true;
                $res['errors'] = $model->errors;
            }

            echo Json::encode($res);
            Yii::$app->end();
        }
    }
    
    /**
     * Deletes an existing Bemor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
                $arrPost = Yii::$app->request->post('ids');
        foreach($arrPost as $ids){
            $this->findModel($ids["id"])->delete();
        }

        echo Json::encode(['status' => 'success']);
        Yii::$app->end();
    }
  
    /**
     * Finds the Bemor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bemor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bemor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
