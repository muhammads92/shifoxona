yii.davoNew = (function($) {
    return {
        isActive: false,
        init : function(){
            using(['form','combobox','linkbutton','combogrid','dialog'],function(){
            	
                $('#davo-new-form').form({
                    success: function (data) {
                        try {
                            data = eval('(' + data + ')');
                            $('#davo-new-form').form('clear');
                            var listNav = document.getElementById('nav-davo-list');
                            if($('#maintab').tabs('exists',listNav.dataset.tabtitle)){
                                $('#maintab').tabs('select',listNav.dataset.tabtitle);
                                $('#davo-index-dg').datagrid('reload');
                            }
                        } catch (e) {
                            if(typeof data !== 'string'){
                                data = e ;
                            }
                            
                            $('#global-error').dialog({
                                title: 'Error',
                                modal: !0,
                                fit:true,
                                content: data
                            });
                        }
                        yii.refreshCsrfToken();
                    }
                });
                
                $('#davo-save-btn').linkbutton({
            		onClick:function(){
            			$('#davo-new-form').form('submit');
            		}
            	});
            	 $('#davo-clear-btn').linkbutton({
            		onClick:function(){
            			$('#davo-new-form').form('clear');
            		}
            	});
            });
        }
    };
})(jQuery);