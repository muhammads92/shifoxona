var davoEditIndex = undefined;
var davoIndexDGIDName = '#davo-index-dg';

yii.davoIndex = (function($) {
    return {
        isActive: false,
        init : function(){
            // using(['datagrid','linkbutton','menubutton','propertygrid','validatebox'],function(){


            function davoEndEditing() {
                if (davoEditIndex == undefined) {
                    return true;
                }
                if ($(davoIndexDGIDName).datagrid('validateRow', davoEditIndex)) {
                    $(davoIndexDGIDName).datagrid('endEdit', davoEditIndex);
                    davoEditIndex = undefined;
                    return true;
                } else {
                    return false;
                }
            }

                    //                 $('#davo-index-del-btn').linkbutton({
                    //     text:'Delete',
                    //     iconCls:'icon-remove',
                    //     plain:!0,
                    //     onClick:function(){
                    //         var checked =  $('#davo-index-dg').datagrid('getChecked'),
                    //             ids=[];
                    //
                    //         if(!checked.length){
                    //             $.messager.alert('Error','Pilih data yang akan di hapus','error');
                    //             return;
                    //         }
                    //
                    //         $.each(checked,function(k,v){
                    //                                             ids.push({id:v.id});
                    //         });
                    //
                    //         $.ajax({
                    //             url:yii.davoIndex.deleteUrl,
                    //             type:'post',
                    //             data:{ids:ids},
                    //             success:function(r){
                    //                 $('#davo-index-dg').datagrid('reload');
                    //             }
                    //         });
                    //     }
                    // });

            var davo_dg = $('#davo-index-dg').edatagrid({
                saveUrl: yii.davoIndex.saveUrl,
                updateUrl: yii.davoIndex.updateUrl,
                destroyUrl: yii.davoIndex.destroyUrl,
                    border:!1,
                    singleSelect:!0,
                    checkOnSelect:!1,
                    selectOnCheck:!1,
                    fit:!0,
                    url:yii.davoIndex.getListDataUrl,
                    pagination:!0,
                    rownumbers: true,
                    remoteFilter: true,
                    method:'get',
                    columns:[[
                        {field:"ck",checkbox:!0},
                        {field:'name',title:'Номи', editor: {type: 'textbox'}},
                        {field:'summa',title:'Нархи', align: 'right', width: '20%', editor: {type: 'numberbox'},
                            formatter:function(value, row, index){return number_format(value, 0, '.', ' ')}
                        },
                        {field:'descraption',title:'Маълумот', editor: {type: 'textbox'}}
                    ]],
                    onBeginEdit: function (index,row) {

                        var dg = $(this);
                        var editors = dg.edatagrid('getEditors', index);
                        console.log(editors);

                        for (var i = 0; i < editors.length; i++) {
                            console.log(editors[i]);

                            console.log(editors[i].target);

                            $(editors[i].target).textbox('textbox').bind('keydown', function (e) {

                                if (e.keyCode == 13) { //enter
                                    //for saving

                                    $('#davo-index-dg').edatagrid('saveRow');
                                }else if(e.keyCode == 27){

                                    $('#davo-index-dg').edatagrid('cancelRow');
                                }
                            });
                        }
                    },
                    onSelect:function(i,row){
                        if(typeof row!=='undefined'){
                            $('#davo-index-pg').propertygrid({
                                data:[
                                {name:'ID',value:row.id,group:'Detail Group',editor:''},
                                {name:'Номи',value:row.name,group:'Detail Group',editor:''},
                                {name:'Нархи',value:row.summa,group:'Detail Group',editor:''},
                                {name:'Маълумот',value:row.descraption,group:'Detail Group',editor:''}
                                ]
                            });
                        }
                    },
                    onLoadSuccess:function(data){
                        yii.app.showGridMsg(this,data);
                        $(this).datagrid('selectRow',0);
                    },
                    onLoadError:function(error){
                        yii.app.showError(error);
                    }
                });

                davo_dg.datagrid('enableFilter');
                
                $('#davo-index-pg').propertygrid({
                    fit:!0,
                    border:!1,
                    showGroup:!0,
                    showHeader:!1
                });
            // });
        }
    };
})(jQuery);