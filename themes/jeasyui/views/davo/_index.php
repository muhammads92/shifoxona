<?php

use app\assets\DavoIndexAsset;
use yii\web\View;
use yii\helpers\Url;
?>

<?php $this->beginPage();?><?php $this->head();?><?php $this->beginBody();?>
<div class="easyui-layout" id="davo-index" fit="true">
    <div region="north" style="height:30px" border="false">
        <div >
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#davo-index-dg').edatagrid('addRow')">Қўшиш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#davo-index-dg').edatagrid('destroyRow')">Ўчириш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#davo-index-dg').edatagrid('saveRow')">Сақлаш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#davo-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>
        </div>
    </div>
    <div region="west" style="width:50%" split="true">
        <div id="davo-index-dg"></div>
    </div>
    <div region="center">
        <div id="davo-index-pg"></div>
    </div>
</div>

<?php $this->endBody();?>
<?php $this->endPage();?>
<?php
$newUrl = Url::to(['davo/new'],true);
$saveUrl = Url::to(['davo/save'],true);
$updateUrl = Url::to(['davo/update'],true);
$destroyUrl = Url::to(['davo/destroy'],true);
$deleteUrl = Url::to(['davo/delete'],true);
$getListDataUrl = Url::to(['davo/get-list-data'],true);
$this->registerJs(<<<EOD
    yii.davoIndex.saveUrl = '{$saveUrl}';
    yii.davoIndex.updateUrl = '{$updateUrl}';
    yii.davoIndex.destroyUrl = '{$destroyUrl}';
    yii.davoIndex.getListDataUrl = '{$getListDataUrl}';
    yii.davoIndex.newUrl = '{$newUrl}';
    yii.davoIndex.deleteUrl = '{$deleteUrl}';
    yii.davoIndex.init();
    yii.easyui.hideMainMask();
EOD
    , View::POS_END );
DavoIndexAsset::register($this);