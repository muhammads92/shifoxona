<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use app\assets\DavoNewAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Davo */
?>

<?php $this->beginPage(); ?>
<?php $this->head(); ?>
<?php $this->beginBody(); ?>
<div class="easyui-layout" fit="true">
    <div region="north" border="false">
        <div class="toolbar">
            <a id="davo-save-btn" class="easyui-linkbutton" icon="icon-save" plain="true">Save</a>
            <a id="davo-clear-btn" class="easyui-linkbutton" icon="icon-cancel" plain="true">Clear</a>
        </div>
    </div>
    <div region="center" border="false" style="padding:5px;">
        <?=Html::beginForm(['davo/new'],'',['id'=>'davo-new-form'])?>
        <table width="100%">
            <tbody>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'name')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'name',['class' =>'easyui-textbox','data-options'=>'required:true','size'=>200]) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'summa')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'summa',['class' =>'easyui-numberbox','data-options'=>'required:true']) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'descraption')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextarea($model, 'descraption',['class' => 'easyui-textbox','data-options'=>'multiline:true,required:true','style'=>'width:300px;height:100px']) ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <?=Html::endForm()?>
    </div>
</div>
<?php $this->endBody(); ?>
<?php $this->endPage(); ?>
<?php
$this->registerJs(<<<EOD
    yii.davoNew.init();
EOD
    , View::POS_END);
DavoNewAsset::register($this);