<?php
use app\assets\BemorIndexAsset;
use app\assets\SiteIndexAsset;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
?>

    <div class="easyui-layout" id="dashboard-index" fit="true">
        <div region="west" style="width:50%" split="true">
            <div>
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#home-index-dg').edatagrid('addRow')">Қўшиш</a>-->
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#home-index-dg').edatagrid('destroyRow')">Ўчириш</a>-->
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#home-index-dg').edatagrid('saveRow')">Сақлаш</a>-->
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#home-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>-->
            </div>
            <div id="site-index-dg"></div>
        </div>
        <div region="center">
            <div class="easyui-layout" data-options="fit:true">
                <div region="north" split="true" style="height:50%;">
                    <div>
<!--                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#home-batafsil-index-dg').edatagrid('addRow')">Қўшиш</a>-->
<!--                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#home-batafsil-index-dg').edatagrid('destroyRow')">Ўчириш</a>-->
<!--                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#home-batafsil-index-dg').edatagrid('saveRow')">Сақлаш</a>-->
<!--                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#home-batafsil-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>-->
                    </div>
<!--                    <div id="davolanish-batafsil-index-dg"></div>-->
                </div>
                <div region="center" split="true" style="height:50%;">
                    <div>
<!--                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#home-tolov-index-dg').edatagrid('addRow')">Қўшиш</a>-->
<!--                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#home-tolov-index-dg').edatagrid('destroyRow')">Ўчириш</a>-->
<!--                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#home-tolov-index-dg').edatagrid('saveRow')">Сақлаш</a>-->
<!--                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#home-tolov-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>-->
                    </div>
<!--                    <div id="davolanish-tolov-index-dg"></div>-->
                </div>
            </div>
        </div>
    </div>

<?php
$newUrl = Url::to(['bemor/new'],true);
$destroyUrl = Url::to(['bemor/destroy'],true);
$saveUrl = Url::to(['bemor/save'],true);
$updateUrl = Url::to(['bemor/update'],true);
$deleteUrl = Url::to(['bemor/delete'],true);
$getListDataUrl = Url::to(['bemor/get-list-data'],true);
$this->registerJs(<<<EOD
    yii.easyui.hideMainMask();
EOD
        , View::POS_END);

SiteIndexAsset::register($this);