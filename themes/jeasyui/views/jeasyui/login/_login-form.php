<?php

use yii\helpers\Html;

/* @var $model backend\modules\user\models\LoginForm */
?>
<?php echo Html::beginForm(['/easyui/login'], 'post', ['id' => 'login-form']) ?>
<table width="100%">
    <tbody>
        <tr>
            <td colspan="3"><i>Логин ва паролни киритинг</i></td>
        </tr>
        <tr>
            <td width="80px"><?php echo Html::activeLabel($model, 'Логин') ?></td>
            <td>:</td>
            <td><?php echo Html::activeTextInput($model, 'username') ?></td>
        </tr>
        <tr>
            <td><?php echo Html::activeLabel($model, 'Парол') ?></td>
            <td>:</td>
            <td><?php echo Html::activeTextInput($model, 'password') ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><?php echo Html::activeCheckbox($model, 'rememberMe') ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
<?php
echo Html::endForm();
