<?php

use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->params['defaultSelectedNav'] = 'nav-dashboard';

return [
    [
        'id' => 'nav-dashboard',
        'text' => 'Бош саҳифа',
        'iconCls' => 'icon-house',
        'attributes' => [
            'url' => Url::to(['/'])
        ]
    ],
    [
        'id' => 'nav-bemor-list',
        'text' => 'Беморлар',
        'iconCls' => 'icon-man',
        'attributes' => [
            'url' => Url::to(['bemor/index'],true)
        ]
    ],
    [
        'id' => 'nav-davo-list',
        'text' => 'Даволаш турлари',
        'iconCls' => 'icon-edit',
        'attributes' => [
            'url' => Url::to(['davo/index'],true)
        ]
    ],
    [
        'id' => 'nav-tolov-list',
        'text' => 'Тўлов',
        'iconCls' => 'icon-sum',
        'attributes' => [
            'url' => Url::to(['tolov/index'],true)
        ]
    ],
    [
        'id' => 'nav-dori-list',
        'text' => 'Дорилар',
        'iconCls' => 'icon-more',
        'attributes' => [
            'url' => Url::to(['dori/index'],true)
        ]
    ],
    [
        'id' => 'nav-davolanish-list',
        'text' => 'Даволаниш',
        'iconCls' => 'icon-table',
        'attributes' => [
            'url' => Url::to(['davolanish/index'],true)
        ]
    ],
//    [
//        'id' => 'nav-batafsil-list',
//        'text' => 'Батафсил',
//        'iconCls' => 'icon-table',
//        'attributes' => [
//            'url' => Url::to(['batafsil/index'],true)
//        ]
//    ],
//    [
//        'text' => 'Demo',
//        'iconCls' => 'icon-demo',
//        'children' => [
//            [
//                'id' => 'nav-demo-application',
//                'text' => 'Application',
//                'iconCls' => 'icon-layout-content',
//                'attributes' => [
//                    'url' => Url::to(['/jeasyui/demo']),
//                ]
//            ]
//        ]
//    ],
//    [
//        'text' => 'Setting',
//        'iconCls' => 'icon-cog',
//        'children' => [
//            [
//                'id' => 'nav-setting',
//                'text' => 'General',
//                'iconCls' => 'icon-cog-edit',
//                'attributes' => [
//                    'url' => Url::to(['/jeasyui/setting']),
//                ]
//            ],
//            [
//                'id' => 'nav-setting-user',
//                'text' => 'Users',
//                'iconCls' => 'icon-group',
//                'attributes' => [
//                    'url' => Url::to(['/jeasyui/setting-user']),
//                ]
//            ],
//            [
//                'id' => 'nav-setting-rbac',
//                'text' => 'Access Management',
//                'iconCls' => 'icon-group-gear',
//                'attributes' => [
//                    'url' => Url::to(['/jeasyui/setting-rbac']),
//                ]
//            ]
//        ]
//    ]
];
