<?php
/* @var $this yii\web\View */
/* @var $model app\models\Davolanish */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <title>Печать</title>

    <style type="text/css">
        body{
            position:relative;

            margin-top: 0mm;
            margin-left: 0mm;
            margin-right: 0mm;
            margin-bottom: 0mm;
            font-weight: 500;
        }
        table{
            padding: 0;
            border-collapse:collapse;
            width: 100%;
            position: relative;
            font-family:"Times New Roman";
            font-size: 3.4mm;
            /*font-weight:bold;*/
        }
        table td {
            padding: 0;
            margin:0;
        }
        table td div{
            padding: 1mm;
        }
        #print{
            padding: 4mm 9mm 4mm 9mm;
        }
        .thead div{
            text-align: center;
        }
        .thead{
            height: 10mm;
        }
        @media print{
            body{
                position:relative;
                /*font-weight:bold;*/
            }
            #print{
                background-color: white;
                width: 100%;
                /*position: fixed;*/
                /*                top: 0;
                                left: 0;
                                margin: 0;*/
                /*padding: 6mm;*/
                font-size: 3.2mm;
                line-height: 3.2mm;
            }
        }
    </style>
    <script type="text/javascript">window.print();</script>
</head>
<body>
<div id="print" style="position:relative; background-color: grey; height:229mm; width: 297mm;">

    <div style="">
        <table border="1" style="width: 90%">
            <tr style="height: 12mm">
                <td rowspan="2" align="center" style="width: 32mm; height: 30mm; padding-left: 2mm; padding-right: 2mm; font-weight: bold;">
                    <div>ЛОГОТИП</div>
                </td>
                <td colspan="3" style="text-align: center; font-weight: bold; height: 6mm;">БЛАНК БЕМОР / <?= $model->id ?></td>
            </tr>
            <tr style="height: 23mm;">
                <td>
                    <div style="padding-left: 10px">ФИШ: <?= $model->bemor->fio ?></div>
                    <div style="padding-left: 10px">Манзил: <?= ($model->bemor->manzil) ?></div>
                    <div style="padding-left: 10px">Ким орқали келган: <?= ($model->came_from) ?></div>
                    <div style="padding-left: 10px">Маьлумот: <?= $model->bemor->descraption ?></div>
                </td>
                <td>
                    <div>Шифохона: ООО ШИФОХОНА</div>
                    <div>Манзил: Юнусобод</div>
                    <div></div><div></div>
                    <div>Даволанган сана: <?= Yii::$app->formatter->asDate($model->date) ?></div>
                </td>
            </tr>
        </table>
    </div>

    <table border="0" style="border:0; height: 12mm; font-size: 4mm; font-weight: bold;">
        <tr>
            <td style="width: 70%; text-align: center;">
                ТЕЛЕФОН "ООО Шифохона": (90) 000 00 00, (71) 000 00 00
            </td>
        </tr>
    </table>

    <table border="1" style="width:90%;">
        <tr class="thead">
            <td><div>№</div></td>
            <td><div>Касаллик тури</div></td>
            <td><div>Нархи</div></td>
            <td><div>Маьлумот</div></td>
        </tr>
        <?php
        $sumAmount = 0;
        $sumCost = 0;
        ?>
        <?php $i=1; foreach($model->batafsils as $batafsil): ?>
        <?php
        $sumAmount += $batafsil->narxi;
        $davo = $batafsil->davo;
        ?>
        <tr>
            <td style="text-align: center;"><div><?= $i; ?></div></td>
            <td><div><?= $davo->name; ?></div></td>
            <td>
                <div style="text-align: right; white-space: nowrap">
                    <?php
                        $a = $batafsil->narxi;
                        $a = Yii::$app->formatter->asDecimal($a, 0);
                        echo $a.' сум';
                    ?>
                </div>
            </td>
            <td><div><?= $davo->descraption; ?></div></td>
            <?php $i++; endforeach; ?>
        </tr>
        <tr>
            <td></td>
            <td><div>ИТОГО:</div></td>
            <td colspan="2">
                <div style="text-align: right; white-space: nowrap;">
                    <?php
                        $a = $model->summa;
                        $a = Yii::$app->formatter->asDecimal($a, 0);
                        echo $a.' сум';
                    ?>
                </div>
            </td>
        </tr>
    </table>
    <br>
    <table border="0">
        <tr>
            <?php $b = 0; foreach($model->tolovs as $tolov): ?>
                <?php $a = $tolov->summa; $b += $a; ?>
            <?php endforeach; ?>
                <td style="text-align: center">Тўлов: </td>
                <td><div style="text-align: left">
                        <?php if ($b == null){
                            echo '0';
                        }else {
                            $b = Yii::$app->formatter->asDecimal($b, 0);
                            echo $b;
                        }?> сум</div></td>
            <td><div></div></td>
            <td style="text-align: center">Қарз: </td>
            <td style="text-align: left">
                <div>
                    <?php $a = $model->qarz;
                    $a = Yii::$app->formatter->asDecimal($a, 0);
                    echo $a; ?> сум
                </div>
            </td>
        </tr>
    </table>

</div>
</body>
</html>