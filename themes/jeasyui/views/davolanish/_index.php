<?php

use app\assets\DavolanishIndexAsset;
use yii\web\View;
use yii\helpers\Url;
?>

<?php $this->beginPage();?><?php $this->head();?><?php $this->beginBody();?>
<div class="easyui-layout" id="davolanish-index" fit="true">
    <div region="west" style="width:50%" split="true" iconCls="icon-ok" title="Даволаниш">
        <div id="davolanish-index-tb">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#davolanish-index-dg').edatagrid('addRow')">Қўшиш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#davolanish-index-dg').edatagrid('destroyRow')">Ўчириш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#davolanish-index-dg').edatagrid('saveRow')">Сақлаш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#davolanish-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>
        </div>
        <div id="davolanish-index-dg"></div>
    </div>
    <div region="center">
        <div class="easyui-layout" data-options="fit:true">
            <div region="north" split="true" style="height:50%;" iconCls="icon-help" title="Батафсил">
                <div id="davolanish-batafsil-index-tb">
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#davolanish-batafsil-index-dg').edatagrid('addRow')">Қўшиш</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#davolanish-batafsil-index-dg').edatagrid('destroyRow')">Ўчириш</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#davolanish-batafsil-index-dg').edatagrid('saveRow')">Сақлаш</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#davolanish-batafsil-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>
                </div>
                <div id="davolanish-batafsil-index-dg"></div>
            </div>
            <div region="center" split="true" style="height:50%;" iconCls="icon-filter" title="Тўлов">
                <div id="davolanish-tolov-index-tb">
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#davolanish-tolov-index-dg').edatagrid('addRow')">Қўшиш</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#davolanish-tolov-index-dg').edatagrid('destroyRow')">Ўчириш</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#davolanish-tolov-index-dg').edatagrid('saveRow')">Сақлаш</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#davolanish-tolov-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>
                </div>
                <div id="davolanish-tolov-index-dg"></div>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody();?>
<?php $this->endPage();?>
<?php
//$newUrl = Url::to(['davolanish/new'],true);
$saveUrl = Url::to(['davolanish/save'],true);
$updateUrl = Url::to(['davolanish/update'],true);
$destroyUrl = Url::to(['davolanish/destroy'],true);
$saveBUrl = Url::to(['davolanish/bsave'],true);
$updateBUrl = Url::to(['davolanish/bupdate'],true);
$destroyBUrl = Url::to(['davolanish/bdestroy'],true);
$saveTUrl = Url::to(['davolanish/tsave'],true);
$updateTUrl = Url::to(['davolanish/tupdate'],true);
$destroyTUrl = Url::to(['davolanish/tdestroy'],true);
$deleteUrl = Url::to(['davolanish/delete'],true);
$getListDataUrl = Url::to(['davolanish/get-list-data'],true);
$getBatafsilListDataUrl = Url::to(['davolanish/get-batafsil-list-data'],true);
$getTolovListDataUrl = Url::to(['davolanish/get-tolov-list-data'],true);
$getBemorDropdownListUrl = Url::to(['bemor/get-dropdown-list'],true);
$getDavoDropdownListUrl = Url::to(['davo/get-dropdown-list'],true);
$getDavolanishQarzUrl = Url::to(['davolanish/get-qarz'],true);
$printUrl = Url::to(['/report/r11'], true);
$this->registerJs(<<<EOD
    yii.davolanishIndex.getListDataUrl = '{$getListDataUrl}';
    yii.davolanishIndex.saveUrl = '{$saveUrl}';
    yii.davolanishIndex.updateUrl = '{$updateUrl}';
    yii.davolanishIndex.destroyUrl = '{$destroyUrl}';
    yii.davolanishIndex.saveBUrl = '{$saveBUrl}';
    yii.davolanishIndex.updateBUrl = '{$updateBUrl}';
    yii.davolanishIndex.destroyBUrl = '{$destroyBUrl}';
    yii.davolanishIndex.saveTUrl = '{$saveTUrl}';
    yii.davolanishIndex.updateTUrl = '{$updateTUrl}';
    yii.davolanishIndex.destroyTUrl = '{$destroyTUrl}';
    yii.davolanishIndex.deleteUrl = '{$deleteUrl}';
    yii.davolanishIndex.getBatafsilListDataUrl = '{$getBatafsilListDataUrl}';
    yii.davolanishIndex.getTolovListDataUrl = '{$getTolovListDataUrl}';
    yii.davolanishIndex.getBemorDropdownListUrl = '{$getBemorDropdownListUrl}';
    yii.davolanishIndex.getDavoDropdownListUrl = '{$getDavoDropdownListUrl}';
    yii.davolanishIndex.getQarzUrl = '{$getDavolanishQarzUrl}';
    yii.davolanishIndex.printUrl = '{$printUrl}';
    yii.davolanishIndex.init();
    yii.easyui.hideMainMask();
EOD
    , View::POS_END );
DavolanishIndexAsset::register($this);