<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use app\assets\DavolanishNewAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Davolanish */
?>

<?php $this->beginPage(); ?>
<?php $this->head(); ?>
<?php $this->beginBody(); ?>
<div class="easyui-layout" fit="true">
    <div region="north" border="false">
        <div class="toolbar">
            <a id="davolanish-save-btn" class="easyui-linkbutton" icon="icon-save" plain="true">Save</a>
            <a id="davolanish-clear-btn" class="easyui-linkbutton" icon="icon-cancel" plain="true">Clear</a>
        </div>
    </div>
    <div region="center" border="false" style="padding:5px;">
        <?=Html::beginForm(['davolanish/new'],'',['id'=>'davolanish-new-form'])?>
        <table width="100%">
            <tbody>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'date')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'date',['class' =>'easyui-datebox','data-options'=>'required:true']) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'bemor_id')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'bemor_id',['class' =>'easyui-numberbox','data-options'=>'required:true']) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'came_from')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'came_from',['class' =>'easyui-textbox','data-options'=>'multiline:true','style'=>'width:300px;height:100px']) ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <?=Html::endForm()?>
    </div>
</div>
<?php $this->endBody(); ?>
<?php $this->endPage(); ?>
<?php
$this->registerJs(<<<EOD
    yii.davolanishNew.init();
EOD
    , View::POS_END);
DavolanishNewAsset::register($this);