yii.davolanishNew = (function($) {
    return {
        isActive: false,
        init : function(){
            // using(['form','combobox','linkbutton','combogrid','dialog'],function(){
            	
                $('#davolanish-new-form').form({
                    success: function (data) {
                        try {
                            data = eval('(' + data + ')');
                            $('#davolanish-new-form').form('clear');
                            var listNav = document.getElementById('nav-davolanish-list');
                            if($('#maintab').tabs('exists',listNav.dataset.tabtitle)){
                                $('#maintab').tabs('select',listNav.dataset.tabtitle);
                                $('#davolanish-index-dg').datagrid('reload');
                            }
                        } catch (e) {
                            if(typeof data !== 'string'){
                                data = e ;
                            }
                            
                            $('#global-error').dialog({
                                title: 'Error',
                                modal: !0,
                                fit:true,
                                content: data
                            });
                        }
                        yii.refreshCsrfToken();
                    }
                });
                
                $('#davolanish-save-btn').linkbutton({
            		onClick:function(){
            			$('#davolanish-new-form').form('submit');
            		}
            	});
            	 $('#davolanish-clear-btn').linkbutton({
            		onClick:function(){
            			$('#davolanish-new-form').form('clear');
            		}
            	});
            // });
        }
    };
})(jQuery);