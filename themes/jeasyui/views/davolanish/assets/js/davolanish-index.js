var davolanishEditIndex = undefined;
var davolanishIndexDGIDName = '#davolanish-index-dg';
var davolanishIndexDGDavolanishId;

yii.davolanishIndex = (function ($) {
    return {
        isActive: false,
        init: function () {

            function davolanishEndEditing() {
                if (davolanishEditIndex == undefined) {
                    return true;
                }
                if ($(davolanishIndexDGIDName).datagrid('validateRow', davolanishEditIndex)) {
                    $(davolanishIndexDGIDName).datagrid('endEdit', davolanishEditIndex);
                    davolanishEditIndex = undefined;
                    return true;
                } else {
                    return false;
                }
            }


//            $('#davolanish-index-del-btn').linkbutton({
//                text: 'Delete',
//                iconCls: 'icon-remove',
//                plain: !0,
//                onClick: function () {
//                    // if (davolanishEditIndex == undefined){return}
//                    // $('#davolanish-index-dg').datagrid('cancelEdit', davolanishEditIndex)
//                    //     .datagrid('deleteRow', davolanishEditIndex);
//                    // davolanishEditIndex = undefined;
//                    var checked = $('#davolanish-index-dg').datagrid('getChecked'),
//                            ids = [];
//
//                    if (!checked.length) {
//                        $.messager.alert('Error', 'Pilih data yang akan di hapus', 'error');
//                        return;
//                    }
//
//                    $.each(checked, function (k, v) {
//                        ids.push({id: v.id});
//                    });
//
//                    $.ajax({
//                        url: yii.davolanishIndex.deleteUrl,
//                        type: 'post',
//                        data: {ids: ids},
//                        success: function (r) {
//                            $('#davolanish-index-dg').datagrid('reload');
//                        }
//                    });
//                }
//            });

            var davolanish_dg = $('#davolanish-index-dg').edatagrid({
                saveUrl: yii.davolanishIndex.saveUrl,
                updateUrl: yii.davolanishIndex.updateUrl,
                destroyUrl: yii.davolanishIndex.destroyUrl,
                border: !1,
                singleSelect: !0,
                checkOnSelect: !1,
                selectOnCheck: !1,
                remoteFilter: true,
                heightAuto: true,
                rownumbers: true,
                toolbar: '#davolanish-index-tb',
                fit: !0,
                url: yii.davolanishIndex.getListDataUrl,
                pagination: !0,
                method: 'get',
                columns: [[
                    {field: "ck", checkbox: !0},
                    {field: 'date', title: 'Сана', editor: {type: 'datebox', options: {formatter:myformatter, parser:myparser}},
                    formatter: function (value, row) {
                        var d = new Date(value);
                        var y = d.getFullYear();
                        var m = d.getMonth()+1;
                        var da = d.getDate();
                        date = y+'-'+m+'-'+d;
                            if (value > date){
                                return '<span style="color:red;">'+value+'</span>';
                            } else {
                                return value;
                            }
                        }
                    },
                    {
                        field: 'bemor_id',
                        title: 'Бемор Ф.И.Ш.',
                        formatter: function (value, row) {
                                return row.fio;
                        },
                        editor: {
                            id: 'dav-bemor-cm',
                            type: 'combobox',
                            options: {
                                valueField: 'id',
                                textField: 'fio',
                                method: 'get',
                                url: yii.davolanishIndex.getBemorDropdownListUrl,
                                required: true
                            }
                        }
                    },
                    {field: 'came_from', title: 'Ким орқали келган', width: '120px', editor: {type: 'textbox'}},
                    {field: 'qarz', title: 'Қарздорлик', align: 'right',
                        formatter: function (value, row, index) {
                            if (value < 0){
                                value = number_format(value, 0, '.', ' ');
                                return '<span style="color:red; font-weight: bold; font-family: serif;">'+value+'</span>';
                            } else {
                                value = number_format(value, 0, '.', ' ');
                                return value;
                            }
                        }
                    },
                    {field: " ", text: 'pechat', title: 'Печат', width: '50px',
                    formatter: function (value,row) {
                        value = '<a target="_blank" href="'+yii.davolanishIndex.printUrl+'?id='+(row.id)+'"><span title="Печат" class="l-btn-left l-btn-icon-left"><span class="l-btn-text"></span><span class="l-btn-icon icon-print">&nbsp;</span></span></a>';
                        return value;
                    }},
                ]],
                onBeginEdit: function (index,row) {

                    var dg = $(this);
                    var editors = dg.edatagrid('getEditors', index);
                    console.log(editors);

                    for (var i = 0; i < editors.length; i++) {
                        console.log(editors[i]);

                        console.log(editors[i].target);

                        $(editors[i].target).textbox('textbox').bind('keydown', function (e) {

                            if (e.keyCode == 13) { //enter
                                //for saving

                                $('#davolanish-index-dg').edatagrid('saveRow');
                            }else if(e.keyCode == 27){

                                $('#davolanish-index-dg').edatagrid('cancelRow');
                            }
                        });
                    }
                },
                onSelect: function (i, row) {

                    if (typeof row.id !== 'undefined') {
                        davolanishIndexDGDavolanishId = row.id;
                    } else {
                        davolanishIndexDGDavolanishId = -1000;
                    }

                    var batafsil_dg = $('#davolanish-batafsil-index-dg');
                    if (!batafsil_dg.data('datagrid')) {
                        // no initialization
                        batafsil_dg.edatagrid({
                            saveUrl: yii.davolanishIndex.saveBUrl,
                            updateUrl: yii.davolanishIndex.updateBUrl,
                            destroyUrl: yii.davolanishIndex.destroyBUrl,
                            border: !1,
                            singleSelect: !0,
                            checkOnSelect: !1,
                            selectOnCheck: !1,
                            toolbar: '#davolanish-batafsil-index-tb',
                            remoteFilter: true,
                            rownumbers: true,
                            fit: !0,
                            url: yii.davolanishIndex.getBatafsilListDataUrl,
                            queryParams: {
                                davolanish_id: davolanishIndexDGDavolanishId
                            },
                            pagination: !0,
                            method: 'get',
                            columns: [[
                                {field: "ck", checkbox: !0},
                                {
                                    field: 'davo_id',
                                    title: 'Касаллик тури',
                                    width: '20%',
                                    formatter: function (value, row) {
                                        return row.name;
                                    },
                                    editor: {
                                        id: 'dav-davo-cm',
                                        type: 'combobox',
                                        options: {
                                            valueField: 'id',
                                            textField: 'name',
                                            method: 'get',
                                            url: yii.davolanishIndex.getDavoDropdownListUrl,
                                            required: true
                                        }
                                    }
                                },
                                {field: 'narxi', title: 'Нархи', align: 'right', width: '20%',
                                    editor: {type: 'numberbox'},
                                    formatter:function(value, row, index){return number_format(value, 0, '.', ' ')}},
                                {field: 'description', title: 'Маьлумот', width: '50%', editor: {type: 'textbox'}}
                            ]],
                             onBeginEdit: function (index,row) {
                                 
                                var dg = $(this);
                                var editors = dg.edatagrid('getEditors', index);
                                
                                for (var i = 0; i < editors.length; i++) {
                                    
                                    $(editors[i].target).textbox('textbox').bind('keydown', function (e) {
                                        
                                        if (e.keyCode == 13) { //enter
                                            //for saving
                                            
                                            $('#davolanish-batafsil-index-dg').edatagrid('saveRow');
                                        }else if(e.keyCode == 27){

                                           $('#davolanish-batafsil-index-dg').edatagrid('cancelRow');
                                        }
                                    });
                                }
                             },
                            onBeforeEdit: function(index,row,changes){

                                row.davolanish_id = davolanishIndexDGDavolanishId;

                                batafsil_dg.edatagrid('updateRow', {
                                    index: index,
                                    row: row
                                });

                            },
                            onEndEdit: function (index, row, changes){
                                var ed = $(this).datagrid('getEditor', {
                                    index: index,
                                    field: 'davo_id'
                                });
                                row.name = $(ed.target).combobox('getText');
                            },
                        }).datagrid('enableFilter');
                    } else {
                        batafsil_dg.edatagrid('load', {
                            davolanish_id: davolanishIndexDGDavolanishId
                        });
                    }

                    var tolov_dg = $('#davolanish-tolov-index-dg');
                    if (!tolov_dg.data('datagrid')) {
                        // no initialization
                        tolov_dg.edatagrid({
                            saveUrl: yii.davolanishIndex.saveTUrl,
                            updateUrl: yii.davolanishIndex.updateTUrl,
                            destroyUrl: yii.davolanishIndex.destroyTUrl,
                            border: !1,
                            singleSelect: !0,
                            checkOnSelect: !1,
                            selectOnCheck: !1,
                            toolbar: '#davolanish-tolov-index-tb',
                            remoteFilter: true,
                            rownumbers: true,
                            fit: !0,
                            url: yii.davolanishIndex.getTolovListDataUrl,
                            queryParams: {
                                davolanish_id: davolanishIndexDGDavolanishId
                            },
                            pagination: !0,
                            method: 'get',
                            columns: [[
                                {field: "ck", checkbox: !0},
                                {field: 'summa', title: 'Нархи', align: 'right', width: '20%', editor: {type: 'numberbox'}, formatter:function(value, row, index){return number_format(value, 0, '.', ' ')}},
                                {field: 'date', title: 'Тўлов қилинган сана', editor: {type: 'datebox', options: {formatter:myformatter, parser:myparser}}},
                                {field: 'discription', title: 'Маьлумот', width: '50%', editor: {type: 'textbox'}}
                            ]],
                            onBeginEdit: function (index,row) {

                                var dg = $(this);
                                var editors = dg.edatagrid('getEditors', index);
                                console.log(editors);

                                for (var i = 0; i < editors.length; i++) {
                                    console.log(editors[i]);

                                    console.log(editors[i].target);

                                    $(editors[i].target).textbox('textbox').bind('keydown', function (e) {

                                        if (e.keyCode == 13) { //enter
                                            //for saving

                                            $('#davolanish-tolov-index-dg').edatagrid('saveRow');
                                        }else if(e.keyCode == 27){

                                            $('#davolanish-tolov-index-dg').edatagrid('cancelRow');
                                        }
                                    });
                                }
                            },
                            onBeforeEdit: function(index,row,changes){

                                row.davolanish_id = davolanishIndexDGDavolanishId;

                                tolov_dg.edatagrid('updateRow', {
                                    index: index,
                                    row: row
                                });

                            },
                        }).datagrid('enableFilter');
                    } else {
                        tolov_dg.edatagrid('load', {
                            davolanish_id: davolanishIndexDGDavolanishId
                        });
                    }

                },
                onLoadSuccess: function (data) {
                    yii.app.showGridMsg(this, data);
                    $(this).datagrid('selectRow', 0);
                },
                onLoadError: function (error) {
                    yii.app.showError(error);
                },
                onEndEdit: function (index, row, changes){
                    var ed = $(this).datagrid('getEditor', {
                        index: index,
                        field: 'bemor_id'
                    });
                    row.fio = $(ed.target).combobox('getText');
                },
//                onAfterEdit: function(index, row, changes){
//                    
//                    
//                },
            });

            davolanish_dg.datagrid('enableFilter');

            $('#davolanish-index-pg').propertygrid({
                fit: !0,
                border: !1,
                showGroup: !0,
                showHeader: !1
            });
            // });
        }
    };
})(jQuery);