yii.batafsilNew = (function($) {
    return {
        isActive: false,
        init : function(){
            using(['form','combobox','linkbutton','combogrid','dialog'],function(){
            	
                $('#batafsil-new-form').form({
                    success: function (data) {
                        try {
                            data = eval('(' + data + ')');
                            $('#batafsil-new-form').form('clear');
                            var listNav = document.getElementById('nav-batafsil-list');
                            if($('#maintab').tabs('exists',listNav.dataset.tabtitle)){
                                $('#maintab').tabs('select',listNav.dataset.tabtitle);
                                $('#batafsil-index-dg').datagrid('reload');
                            }
                        } catch (e) {
                            if(typeof data !== 'string'){
                                data = e ;
                            }
                            
                            $('#global-error').dialog({
                                title: 'Error',
                                modal: !0,
                                fit:true,
                                content: data
                            });
                        }
                        yii.refreshCsrfToken();
                    }
                });
                
                $('#batafsil-save-btn').linkbutton({
            		onClick:function(){
            			$('#batafsil-new-form').form('submit');
            		}
            	});
            	 $('#batafsil-clear-btn').linkbutton({
            		onClick:function(){
            			$('#batafsil-new-form').form('clear');
            		}
            	});
            });
        }
    };
})(jQuery);