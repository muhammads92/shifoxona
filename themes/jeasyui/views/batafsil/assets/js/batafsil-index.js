yii.batafsilIndex = (function($) {
    return {
        isActive: false,
        init : function(){
            // using(['datagrid','linkbutton','menubutton','propertygrid','validatebox'],function(){
                $('#batafsil-index-new-btn').linkbutton({
                    text:'Қўшиш',
                    iconCls:'icon-add',
                    plain:!0,
                    onClick:function(){
                        yii.app.createTab('Batafsil New',yii.batafsilIndex.newUrl,'ia-icon-form','nav-batafsil-new')
                    }
                });
                
                                    $('#batafsil-index-del-btn').linkbutton({
                        text:'Ўчириш',
                        iconCls:'icon-remove',
                        plain:!0,
                        onClick:function(){
                            var checked =  $('#batafsil-index-dg').datagrid('getChecked'),
                                ids=[];

                            if(!checked.length){
                                $.messager.alert('Error','Pilih data yang akan di hapus','error');
                                return;
                            }

                            $.each(checked,function(k,v){
                                                                ids.push({id:v.id});
                            });

                            $.ajax({
                                url:yii.batafsilIndex.deleteUrl,
                                type:'post',
                                data:{ids:ids},
                                success:function(r){
                                    $('#batafsil-index-dg').datagrid('reload');
                                }
                            });
                        }
                    });
                                
                $('#batafsil-index-dg').datagrid({
                    border:!1,
                    singleSelect:!0,
                    checkOnSelect:!1,
                    selectOnCheck:!1,
                    fit:!0,
                    url:yii.batafsilIndex.getListDataUrl,
                    pagination:!0,
                    method:'get',
                    rownumbers:true,
                    columns:[[
                        {field:"ck",checkbox:!0},
                        {field:'davolanish_id',title:'Бемор Ф.И.Ш.'},
                        {field:'davo_id',title:'Касаллик тури'},
                        {field:'narxi',title:'Нархи'},
                        {field:'description',title:'Маълумот'}
                    ]],
                    onSelect:function(i,row){
                        if(typeof row!=='undefined'){
                            $('#batafsil-index-pg').propertygrid({
                                data:[
                                    {name:'ID',value:row.id,group:'Detail Group',editor:''},
                                {name:'Бемор Ф.И.Ш.',value:row.davolanish_id,group:'Detail Group',editor:''},
                                {name:'Касаллик тури',value:row.davo_id,group:'Detail Group',editor:''},
                                {name:'Нархи',value:row.narxi,group:'Detail Group',editor:''},
                                {name:'Маълумот',value:row.description,group:'Detail Group',editor:''}
                                ]
                            });
                        }
                    },
                    onLoadSuccess:function(data){
                        yii.app.showGridMsg(this,data);
                        $(this).datagrid('selectRow',0);
                    },
                    onLoadError:function(error){
                        yii.app.showError(error);
                    }
                });
                
                $('#batafsil-index-pg').propertygrid({
                    fit:!0,
                    border:!1,
                    showGroup:!0,
                    showHeader:!1
                });
            // });
        }
    };
})(jQuery);