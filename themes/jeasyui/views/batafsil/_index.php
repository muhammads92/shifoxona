<?php

use app\assets\BatafsilIndexAsset;
use yii\web\View;
use yii\helpers\Url;
?>

<?php $this->beginPage();?><?php $this->head();?><?php $this->beginBody();?>
<div class="easyui-layout" id="batafsil-index" fit="true">
    <div region="north" style="height:30px" border="false">
        <div >
            <a id="batafsil-index-new-btn">&nbsp;</a>
            <a id="batafsil-index-del-btn">&nbsp;</a>
        </div>
    </div>
    <div region="west" style="width:50%" split="true">
        <div id="batafsil-index-dg"></div>
    </div>
    <div region="center">
        <div id="batafsil-index-pg"></div>
    </div>
</div>

<?php $this->endBody();?>
<?php $this->endPage();?>
<?php
$newUrl = Url::to(['batafsil/new'],true);
$deleteUrl = Url::to(['batafsil/delete'],true);
$getListDataUrl = Url::to(['batafsil/get-list-data'],true);
$this->registerJs(<<<EOD
    yii.batafsilIndex.getListDataUrl = '{$getListDataUrl}';
    yii.batafsilIndex.newUrl = '{$newUrl}';
    yii.batafsilIndex.deleteUrl = '{$deleteUrl}';
    yii.batafsilIndex.init();
    yii.easyui.hideMainMask();
EOD
    , View::POS_END );
BatafsilIndexAsset::register($this);