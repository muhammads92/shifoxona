<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use app\assets\BatafsilNewAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Batafsil */
?>

<?php $this->beginPage(); ?>
<?php $this->head(); ?>
<?php $this->beginBody(); ?>
<div class="easyui-layout" fit="true">
    <div region="north" border="false">
        <div class="toolbar">
            <a id="batafsil-save-btn" class="easyui-linkbutton" icon="icon-save" plain="true">Save</a>
            <a id="batafsil-clear-btn" class="easyui-linkbutton" icon="icon-cancel" plain="true">Clear</a>
        </div>
    </div>
    <div region="center" border="false" style="padding:5px;">
        <?=Html::beginForm(['batafsil/new'],'',['id'=>'batafsil-new-form'])?>
        <table width="100%">
            <tbody>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'davolanish_id')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'davolanish_id',['class' =>'easyui-numberbox','data-options'=>'required:true']) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'davo_id')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'davo_id',['class' =>'easyui-numberbox','data-options'=>'required:true']) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'narxi')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'narxi',['class' =>'easyui-numberbox','data-options'=>'required:true']) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'description')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextarea($model, 'description',['class' => 'easyui-textbox','data-options'=>'multiline:true,required:true','style'=>'width:300px;height:100px']) ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <?=Html::endForm()?>
    </div>
</div>
<?php $this->endBody(); ?>
<?php $this->endPage(); ?>
<?php
$this->registerJs(<<<EOD
    yii.batafsilNew.init();
EOD
    , View::POS_END);
BatafsilNewAsset::register($this);