<?php

use app\assets\BemorIndexAsset;
use yii\web\View;
use yii\helpers\Url;
?>

<?php $this->beginPage();?><?php $this->head();?><?php $this->beginBody();?>
<div class="easyui-layout" id="bemor-index" fit="true">
    <div region="north" style="height:30px" border="false">
        <div >
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#bemor-index-dg').edatagrid('addRow')">Қўшиш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#bemor-index-dg').edatagrid('destroyRow')">Ўчириш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#bemor-index-dg').edatagrid('saveRow')">Сақлаш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#bemor-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>
        </div>
    </div>
    <div region="west" style="width:50%" split="true">
        <div id="bemor-index-dg"></div>
    </div>
    <div region="center">
        <div id="bemor-index-pg"></div>
    </div>
</div>

<?php $this->endBody();?>
<?php $this->endPage();?>
<?php
$newUrl = Url::to(['bemor/new'],true);
$destroyUrl = Url::to(['bemor/destroy'],true);
$saveUrl = Url::to(['bemor/save'],true);
$updateUrl = Url::to(['bemor/update'],true);
$deleteUrl = Url::to(['bemor/delete'],true);
$getListDataUrl = Url::to(['bemor/get-list-data'],true);
$this->registerJs(<<<EOD
    yii.bemorIndex.saveUrl = '{$saveUrl}';
    yii.bemorIndex.updateUrl = '{$updateUrl}';
    yii.bemorIndex.destroyUrl = '{$destroyUrl}';
    yii.bemorIndex.getListDataUrl = '{$getListDataUrl}';
    yii.bemorIndex.newUrl = '{$newUrl}';
    yii.bemorIndex.deleteUrl = '{$deleteUrl}';
    yii.bemorIndex.init();
    yii.easyui.hideMainMask();
EOD
    , View::POS_END );
BemorIndexAsset::register($this);