yii.bemorNew = (function($) {
    return {
        isActive: false,
        init : function(){
            using(['form','combobox','linkbutton','combogrid','dialog'],function(){
            	
                $('#bemor-new-form').form({
                    success: function (data) {
                        try {
                            data = eval('(' + data + ')');
                            $('#bemor-new-form').form('clear');
                            var listNav = document.getElementById('nav-bemor-list');
                            if($('#maintab').tabs('exists',listNav.dataset.tabtitle)){
                                $('#maintab').tabs('select',listNav.dataset.tabtitle);
                                $('#bemor-index-dg').datagrid('reload');
                            }
                        } catch (e) {
                            if(typeof data !== 'string'){
                                data = e ;
                            }
                            
                            $('#global-error').dialog({
                                title: 'Error',
                                modal: !0,
                                fit:true,
                                content: data
                            });
                        }
                        yii.refreshCsrfToken();
                    }
                });
                
                $('#bemor-save-btn').linkbutton({
            		onClick:function(){
            			$('#bemor-new-form').form('submit');
            		}
            	});
            	 $('#bemor-clear-btn').linkbutton({
            		onClick:function(){
            			$('#bemor-new-form').form('clear');
            		}
            	});
            });
        }
    };
})(jQuery);