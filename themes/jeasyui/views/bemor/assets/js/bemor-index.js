var bemorEditIndex = undefined;
var bemorIndexDGIDName = '#bemor-index-dg';

yii.bemorIndex = (function($) {
    return {
        isActive: false,
        init : function(){
            // using(['datagrid','linkbutton','menubutton','propertygrid','validatebox'],function(){

            function bemorEndEditing() {
                if (bemorEditIndex == undefined) {
                    return true;
                }
                if ($(bemorIndexDGIDName).datagrid('validateRow', bemorEditIndex)) {
                    $(bemorIndexDGIDName).datagrid('endEdit', bemorEditIndex);
                    bemorEditIndex = undefined;
                    return true;
                } else {
                    return false;
                }
            }

                
                    //                 $('#bemor-index-del-btn').linkbutton({
                    //     text:'Ўчириш',
                    //     iconCls:'icon-remove',
                    //     plain:!0,
                    //     onClick:function(){
                    //         var checked =  $('#bemor-index-dg').datagrid('getChecked'),
                    //             ids=[];
                    //
                    //         if(!checked.length){
                    //             $.messager.alert('Error','Pilih data yang akan di hapus','error');
                    //             return;
                    //         }
                    //
                    //         $.each(checked,function(k,v){
                    //                                             ids.push({id:v.id});
                    //         });
                    //
                    //         $.ajax({
                    //             url:yii.bemorIndex.deleteUrl,
                    //             type:'post',
                    //             data:{ids:ids},
                    //             success:function(r){
                    //                 $('#bemor-index-dg').datagrid('reload');
                    //             }
                    //         });
                    //     }
                    // });

                var bemor_dg = $('#bemor-index-dg').edatagrid({
                    saveUrl: yii.bemorIndex.saveUrl,
                    updateUrl: yii.bemorIndex.updateUrl,
                    destroyUrl: yii.bemorIndex.destroyUrl,
                    border:!1,
                    singleSelect:!0,
                    checkOnSelect:!1,
                    selectOnCheck:!1,
                    fit:!0,
                    url:yii.bemorIndex.getListDataUrl,
                    pagination:!0,
                    method:'get',
                    remoteFilter: true,
                    rownumbers: true,
                    columns:[[
                        {field:"ck",checkbox:!0},
                        {field:'fio',title:'Ф.И.Ш.', editor: {type: 'textbox'}},
                        {field:'manzil',title:'Манзил', editor: {type: 'textbox'}},
                        {field:'descraption',title:'Маълумот', editor: {type: 'textbox'}},
                        {field:'active',title:'Бемор актив', editor: {type: 'numberbox'}}
                    ]],
                    onBeginEdit: function (index,row) {

                        var dg = $(this);
                        var editors = dg.edatagrid('getEditors', index);
                        console.log(editors);

                        for (var i = 0; i < editors.length; i++) {
                            console.log(editors[i]);

                            console.log(editors[i].target);

                            $(editors[i].target).textbox('textbox').bind('keydown', function (e) {

                                if (e.keyCode == 13) { //enter
                                    //for saving

                                    $('#bemor-index-dg').edatagrid('saveRow');
                                }else if(e.keyCode == 27){

                                    $('#bemor-index-dg').edatagrid('cancelRow');
                                }
                            });
                        }
                    },
                    onSelect:function(i,row){
                        if(typeof row!=='undefined'){
                            $('#bemor-index-pg').propertygrid({
                                data:[
                                {name:'ID',value:row.id,group:'Detail Group',editor:''},
                                {name:'Ф.И.Ш.',value:row.fio,group:'Detail Group',editor:''},
                                {name:'Манзил',value:row.manzil,group:'Detail Group',editor:''},
                                {name:'Маълумот',value:row.descraption,group:'Detail Group',editor:''},
                                {name:'Бемор актив',value:row.active,group:'Detail Group',editor:''}
                                ]
                            });
                        }
                    },
                    onLoadSuccess:function(data){
                        yii.app.showGridMsg(this,data);
                        $(this).datagrid('selectRow',0);
                    },
                    onLoadError:function(error){
                        yii.app.showError(error);
                    }
                });
            
                bemor_dg.datagrid('enableFilter');
                    
                $('#bemor-index-pg').propertygrid({
                    fit:!0,
                    border:!1,
                    showGroup:!0,
                    showHeader:!1
                });
            // });
        }
    };
})(jQuery);