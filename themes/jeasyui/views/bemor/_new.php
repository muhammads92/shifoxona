<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use app\assets\BemorNewAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Bemor */
?>

<?php $this->beginPage(); ?>
<?php $this->head(); ?>
<?php $this->beginBody(); ?>
<div class="easyui-layout" fit="true">
    <div region="north" border="false">
        <div class="toolbar">
            <a id="bemor-save-btn" class="easyui-linkbutton" icon="icon-save" plain="true">Save</a>
            <a id="bemor-clear-btn" class="easyui-linkbutton" icon="icon-cancel" plain="true">Clear</a>
        </div>
    </div>
    <div region="center" border="false" style="padding:5px;">
        <?=Html::beginForm(['bemor/new'],'',['id'=>'bemor-new-form'])?>
        <table width="100%">
            <tbody>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'fio')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'fio',['class' =>'easyui-textbox','data-options'=>'required:true','size'=>200]) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'manzil')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'manzil',['class' =>'easyui-textbox','data-options'=>'required:true','size'=>200]) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'descraption')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextarea($model, 'descraption',['class' => 'easyui-textbox','data-options'=>'multiline:true,required:true','style'=>'width:300px;height:100px']) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'active')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'active',['class' =>'easyui-textbox','data-options'=>'required:true','size'=>4]) ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <?=Html::endForm()?>
    </div>
</div>
<?php $this->endBody(); ?>
<?php $this->endPage(); ?>
<?php
$this->registerJs(<<<EOD
    yii.bemorNew.init();
EOD
    , View::POS_END);
BemorNewAsset::register($this);