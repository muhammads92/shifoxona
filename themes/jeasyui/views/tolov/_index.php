<?php

use app\assets\TolovIndexAsset;
use yii\web\View;
use yii\helpers\Url;
?>

<?php $this->beginPage();?><?php $this->head();?><?php $this->beginBody();?>
<div class="easyui-layout" id="tolov-index" fit="true">
    <div region="north" style="height:30px" border="false">
        <div >
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#tolov-index-dg').edatagrid('addRow')">Қўшиш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tolov-index-dg').edatagrid('destroyRow')">Ўчириш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tolov-index-dg').edatagrid('saveRow')">Сақлаш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tolov-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>
        </div>
    </div>
    <div region="west" style="width:50%" split="true">
        <div id="tolov-index-dg"></div>
    </div>
    <div region="center">
        <div id="tolov-index-pg"></div>
    </div>
</div>

<?php $this->endBody();?>
<?php $this->endPage();?>
<?php
$newUrl = Url::to(['tolov/new'],true);
$destroyUrl = Url::to(['tolov/destroy'],true);
$saveUrl = Url::to(['tolov/save'],true);
$updateUrl = Url::to(['tolov/update'],true);
$deleteUrl = Url::to(['tolov/delete'],true);
$getListDataUrl = Url::to(['tolov/get-list-data'],true);
$this->registerJs(<<<EOD
    yii.tolovIndex.saveUrl = '{$saveUrl}';
    yii.tolovIndex.updateUrl = '{$updateUrl}';
    yii.tolovIndex.destroyUrl = '{$destroyUrl}';
    yii.tolovIndex.getListDataUrl = '{$getListDataUrl}';
    yii.tolovIndex.newUrl = '{$newUrl}';
    yii.tolovIndex.deleteUrl = '{$deleteUrl}';
    yii.tolovIndex.init();
    yii.easyui.hideMainMask();
EOD
    , View::POS_END );
TolovIndexAsset::register($this);