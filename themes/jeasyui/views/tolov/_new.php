<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use app\assets\TolovNewAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Tolov */
?>

<?php $this->beginPage(); ?>
<?php $this->head(); ?>
<?php $this->beginBody(); ?>
<div class="easyui-layout" fit="true">
    <div region="north" border="false">
        <div class="toolbar">
            <a id="tolov-save-btn" class="easyui-linkbutton" icon="icon-save" plain="true">Save</a>
            <a id="tolov-clear-btn" class="easyui-linkbutton" icon="icon-cancel" plain="true">Clear</a>
        </div>
    </div>
    <div region="center" border="false" style="padding:5px;">
        <?=Html::beginForm(['tolov/new'],'',['id'=>'tolov-new-form'])?>
        <table width="100%">
            <tbody>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'davolanish_id')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'davolanish_id',['class' =>'easyui-numberbox','data-options'=>'required:true']) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'summa')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'summa',['class' =>'easyui-numberbox','data-options'=>'required:true']) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'date')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextInput($model, 'date',['class' =>'easyui-datebox','data-options'=>'required:true']) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeLabel($model, 'discription')?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Html::activeTextarea($model, 'discription',['class' => 'easyui-textbox','data-options'=>'multiline:true','style'=>'width:300px;height:100px']) ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <?=Html::endForm()?>
    </div>
</div>
<?php $this->endBody(); ?>
<?php $this->endPage(); ?>
<?php
$this->registerJs(<<<EOD
    yii.tolovNew.init();
    yii.easyui.hideMainMask();
EOD
    , View::POS_END);
TolovNewAsset::register($this);