yii.tolovNew = (function($) {
    return {
        isActive: false,
        init : function(){
            // using(['form','combobox','linkbutton','combogrid','dialog'],function(){
            	
                $('#tolov-new-form').form({
                    success: function (data) {
                        try {
                            data = eval('(' + data + ')');
                            $('#tolov-new-form').form('clear');
                            var listNav = document.getElementById('nav-tolov-list');
                            if($('#maintab').tabs('exists',listNav.dataset.tabtitle)){
                                $('#maintab').tabs('select',listNav.dataset.tabtitle);
                                $('#tolov-index-dg').datagrid('reload');
                            }
                        } catch (e) {
                            if(typeof data !== 'string'){
                                data = e ;
                            }
                            
                            $('#global-error').dialog({
                                title: 'Error',
                                modal: !0,
                                fit:true,
                                content: data
                            });
                        }
                        yii.refreshCsrfToken();
                    }
                });
                
                $('#tolov-save-btn').linkbutton({
            		onClick:function(){
            			$('#tolov-new-form').form('submit');
            		}
            	});
            	 $('#tolov-clear-btn').linkbutton({
            		onClick:function(){
            			$('#tolov-new-form').form('clear');
            		}
            	});
            // });
        }
    };
})(jQuery);