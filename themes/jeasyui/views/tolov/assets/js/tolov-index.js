var tolovEditIndex = undefined;
var tolovIndexDGIDName = '#tolov-index-dg';

yii.tolovIndex = (function($) {
    return {
        isActive: false,
        init : function(){
            // using(['datagrid','linkbutton','menubutton','propertygrid','validatebox'],function(){

            function tolovEndEditing() {
                if (tolovEditIndex == undefined) {
                    return true;
                }
                if ($(tolovIndexDGIDName).datagrid('validateRow', tolovEditIndex)) {
                    $(tolovIndexDGIDName).datagrid('endEdit', tolovEditIndex);
                    tolovEditIndex = undefined;
                    return true;
                } else {
                    return false;
                }
            }
                
                    //                 $('#tolov-index-del-btn').linkbutton({
                    //     text:'Ўчириш',
                    //     iconCls:'icon-remove',
                    //     plain:!0,
                    //     onClick:function(){
                    //         var checked =  $('#tolov-index-dg').datagrid('getChecked'),
                    //             ids=[];
                    //
                    //         if(!checked.length){
                    //             $.messager.alert('Error','Pilih data yang akan di hapus','error');
                    //             return;
                    //         }
                    //
                    //         $.each(checked,function(k,v){
                    //                                             ids.push({id:v.id});
                    //         });
                    //
                    //         $.ajax({
                    //             url:yii.tolovIndex.deleteUrl,
                    //             type:'post',
                    //             data:{ids:ids},
                    //             success:function(r){
                    //                 $('#tolov-index-dg').datagrid('reload');
                    //             }
                    //         });
                    //     }
                    // });

                var tolov_dg = $('#tolov-index-dg').edatagrid({
                    saveUrl: yii.tolovIndex.saveUrl,
                    updateUrl: yii.tolovIndex.updateUrl,
                    destroyUrl: yii.tolovIndex.destroyUrl,
                    border:!1,
                    singleSelect:!0,
                    checkOnSelect:!1,
                    selectOnCheck:!1,
                    fit:!0,
                    url:yii.tolovIndex.getListDataUrl,
                    pagination:!0,
                    method:'get',
                    rownumbers: true,
                    columns:[[
                        {field:"ck",checkbox:!0},
                        {field:'davolanish_id', title:'Даволаниш', editor: {type: 'numberbox'}},
                        {field:'summa',title:'Сумма', align: 'right', width: '20%', editor: {type: 'numberbox'},
                            formatter:function(value, row, index){return number_format(value, 0, '.', ' ')}
                        },
                        {field:'date',title:'Тўлов қилинган сана', editor: {type: 'datebox', options: {formatter:myformatter, parser:myparser}}},
                        {field:'discription',title:'Маълумот', editor: {type: 'textbox'}}
                    ]],
                    onBeginEdit: function (index,row) {

                        var dg = $(this);
                        var editors = dg.edatagrid('getEditors', index);
                        console.log(editors);

                        for (var i = 0; i < editors.length; i++) {
                            console.log(editors[i]);

                            console.log(editors[i].target);

                            $(editors[i].target).textbox('textbox').bind('keydown', function (e) {

                                if (e.keyCode == 13) { //enter
                                    //for saving

                                    $('#tolov-index-dg').edatagrid('saveRow');
                                }else if(e.keyCode == 27){

                                    $('#tolov-index-dg').edatagrid('cancelRow');
                                }
                            });
                        }
                    },
                    onSelect:function(i,row){
                        if(typeof row!=='undefined'){
                            $('#tolov-index-pg').propertygrid({
                                data:[
                                {name:'ID',value:row.id,group:'Detail Group',editor:''},
                                {name:'Даволаниш',value:row.davolanish_id,group:'Detail Group',editor:''},
                                {name:'Сумма',value:row.summa,group:'Detail Group',editor:''},
                                {name:'Тўлов қилинган сана',value:row.date,group:'Detail Group',editor:''},
                                {name:'Маълумот',value:row.discription,group:'Detail Group',editor:''}
                                ]
                            });
                        }
                    },
                    onLoadSuccess:function(data){
                        yii.app.showGridMsg(this,data);
                        $(this).datagrid('selectRow',0);
                    },
                    onLoadError:function(error){
                        yii.app.showError(error);
                    }
                });

                tolov_dg.datagrid('enableFilter');
                
                $('#tolov-index-pg').propertygrid({
                    fit:!0,
                    border:!1,
                    showGroup:!0,
                    showHeader:!1
                });
            // });
        }
    };
})(jQuery);