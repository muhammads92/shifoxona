yii.doriNew = (function($) {
    return {
        isActive: false,
        init : function(){
            using(['form','combobox','linkbutton','combogrid','dialog'],function(){
            	
                $('#dori-new-form').form({
                    success: function (data) {
                        try {
                            data = eval('(' + data + ')');
                            $('#dori-new-form').form('clear');
                            var listNav = document.getElementById('nav-dori-list');
                            if($('#maintab').tabs('exists',listNav.dataset.tabtitle)){
                                $('#maintab').tabs('select',listNav.dataset.tabtitle);
                                $('#dori-index-dg').datagrid('reload');
                            }
                        } catch (e) {
                            if(typeof data !== 'string'){
                                data = e ;
                            }
                            
                            $('#global-error').dialog({
                                title: 'Error',
                                modal: !0,
                                fit:true,
                                content: data
                            });
                        }
                        yii.refreshCsrfToken();
                    }
                });
                
                $('#dori-save-btn').linkbutton({
            		onClick:function(){
            			$('#dori-new-form').form('submit');
            		}
            	});
            	 $('#dori-clear-btn').linkbutton({
            		onClick:function(){
            			$('#dori-new-form').form('clear');
            		}
            	});
            });
        }
    };
})(jQuery);