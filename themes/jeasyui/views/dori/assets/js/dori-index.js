var doriEditIndex = undefined;
var doriIndexDGIDName = '#dori-index-dg';
var doriIndexDGDoriId;
// var balance = yii.doriIndex.getBalanceUrl;

yii.doriIndex = (function($) {
    return {
        isActive: false,
        init : function(){
            // using(['datagrid','linkbutton','menubutton','propertygrid','validatebox'],function(){
            function doriEndEditing() {
                if (doriEditIndex == undefined) {
                    return true;
                }
                if ($(doriIndexDGIDName).datagrid('validateRow', doriEditIndex)) {
                    $(doriIndexDGIDName).datagrid('endEdit', doriEditIndex);
                    doriEditIndex = undefined;
                    return true;
                } else {
                    return false;
                }
            }

            var dori_dg = $('#dori-index-dg').edatagrid({
                saveUrl: yii.doriIndex.saveUrl,
                updateUrl: yii.doriIndex.updateUrl,
                destroyUrl: yii.doriIndex.destroyUrl,
                border:!1,
                singleSelect:!0,
                checkOnSelect:!1,
                selectOnCheck:!1,
                toolbar: '#dori-index-tb',
                remoteFilter: true,
                rownumbers: true,
                fit:!0,
                url:yii.doriIndex.getListDataUrl,
                pagination:!0,
                method:'get',
                columns:[[
                    {field:"ck",checkbox:!0},
                    {field:'name',title:'Номи', width:120, editor: {type: 'textbox'}},
                    {field:'descraption',title:'Маълумот', width:200, editor: {type: 'textbox'}},
                    {field:'summa',title:'Дори нархи', align:'right', editor: {type: 'numberbox'},
                        formatter:function(value, row, index){return number_format(value, 0, '.', ' ')}
                    },
                    {field: 'balance', title: 'Қолдиқ', align: 'right',
                        formatter: function (value, row) {
                            if (value < 0){
                                value = number_format(value, 0, '.', ' ');
                                return '<span style="color:red; font-weight: bold; font-family: serif;">'+value+'</span>';
                            } else {
                                value = number_format(value, 0, '.', ' ');
                                return value;
                            }
                        }}
                ]],
                onBeginEdit: function (index,row) {

                    var dg = $(this);
                    var editors = dg.edatagrid('getEditors', index);
                    console.log(editors);

                    for (var i = 0; i < editors.length; i++) {
                        console.log(editors[i]);

                        console.log(editors[i].target);

                        $(editors[i].target).textbox('textbox').bind('keydown', function (e) {

                            if (e.keyCode == 13) { //enter
                                //for saving

                                $('#dori-index-dg').edatagrid('saveRow');
                            }else if(e.keyCode == 27){

                                $('#dori-index-dg').edatagrid('cancelRow');
                            }
                        });
                    }
                },
                onSelect:function(i,row){
                    if (typeof row.id !== 'undefined') {
                        doriIndexDGDoriId = row.id;
                    } else {
                        doriIndexDGDoriId = -1000;
                    }

                    var prixod_dg = $('#dori-prixod-index-dg');
                    if (!prixod_dg.data('datagrid')) {
                        // no initialization
                        prixod_dg.edatagrid({
                            saveUrl: yii.doriIndex.savePUrl,
                            updateUrl: yii.doriIndex.updatePUrl,
                            destroyUrl: yii.doriIndex.destroyPUrl,
                            border: !1,
                            singleSelect: !0,
                            checkOnSelect: !1,
                            selectOnCheck: !1,
                            toolbar: '#dori-prixod-index-tb',
                            remoteFilter: true,
                            rownumbers: true,
                            fit: !0,
                            url: yii.doriIndex.getPrixodListDataUrl,
                            queryParams: {
                                dori_id: doriIndexDGDoriId
                            },
                            pagination: !0,
                            method: 'get',
                            columns: [[
                                {field: "ck", checkbox: !0},
                                {field: 'amound', title: 'Сони', align: 'right', width:70, editor: {type: 'numberbox'}},
                                {field: 'date', title: 'Сана', align: 'center', width:90, editor: {type: 'datebox', options: {formatter:myformatter, parser:myparser}}}
                            ]],
                            onBeginEdit: function (index,row) {

                                var dg = $(this);
                                var editors = dg.edatagrid('getEditors', index);
                                console.log(editors);

                                for (var i = 0; i < editors.length; i++) {
                                    console.log(editors[i]);

                                    console.log(editors[i].target);

                                    $(editors[i].target).textbox('textbox').bind('keydown', function (e) {

                                        if (e.keyCode == 13) { //enter
                                            //for saving

                                            $('#dori-prixod-index-dg').edatagrid('saveRow');
                                        }else if(e.keyCode == 27){

                                            $('#dori-prixod-index-dg').edatagrid('cancelRow');
                                        }
                                    });
                                }
                            },
                            onBeforeEdit: function(index,row,changes){

                                row.dori_id = doriIndexDGDoriId;

                                prixod_dg.edatagrid('updateRow', {
                                    index: index,
                                    row: row
                                });

                            },
                        }).datagrid('enableFilter');
                    } else {
                        prixod_dg.edatagrid('load', {
                            dori_id: doriIndexDGDoriId
                        });
                    }

                    var rasxod_dg = $('#dori-rasxod-index-dg');
                    if (!rasxod_dg.data('datagrid')) {
                        // no initialization
                        rasxod_dg.edatagrid({
                            saveUrl: yii.doriIndex.saveRUrl,
                            updateUrl: yii.doriIndex.updateRUrl,
                            destroyUrl: yii.doriIndex.destroyRUrl,
                            border: !1,
                            singleSelect: !0,
                            checkOnSelect: !1,
                            selectOnCheck: !1,
                            toolbar: '#dori-rasxod-index-tb',
                            remoteFilter: true,
                            rownumbers: true,
                            fit: !0,
                            url: yii.doriIndex.getRasxodListDataUrl,
                            queryParams: {
                                dori_id: doriIndexDGDoriId
                            },
                            pagination: !0,
                            method: 'get',
                            columns: [[
                                {field: "ck", checkbox: !0},
                                {field: 'amound', title: 'Сони', align: 'right', width:70, editor: {type: 'numberbox'}},
                                {field: 'date', title: 'Сана', align: 'center', width:90, editor: {type: 'datebox', options: {formatter:myformatter, parser:myparser}}}
                            ]],
                            onBeginEdit: function (index,row) {

                                var dg = $(this);
                                var editors = dg.edatagrid('getEditors', index);
                                console.log(editors);

                                for (var i = 0; i < editors.length; i++) {
                                    console.log(editors[i]);

                                    console.log(editors[i].target);

                                    $(editors[i].target).textbox('textbox').bind('keydown', function (e) {

                                        if (e.keyCode == 13) { //enter
                                            //for saving

                                            $('#dori-rasxod-index-dg').edatagrid('saveRow');
                                        }else if(e.keyCode == 27){

                                            $('#dori-rasxod-index-dg').edatagrid('cancelRow');
                                        }
                                    });
                                }
                            },
                            onBeforeEdit: function(index,row,changes){

                                row.dori_id = doriIndexDGDoriId;

                                rasxod_dg.edatagrid('updateRow', {
                                    index: index,
                                    row: row
                                });

                            },
                        }).datagrid('enableFilter');
                    } else {
                        rasxod_dg.edatagrid('load', {
                            dori_id: doriIndexDGDoriId
                        });
                    }

                },
                    onLoadSuccess:function(data){
                        yii.app.showGridMsg(this,data);
                        $(this).datagrid('selectRow',0);
                    },
                    onLoadError:function(error){
                        yii.app.showError(error);
                    }
                });

                dori_dg.datagrid('enableFilter');

                $('#dori-index-pg').propertygrid({
                    fit:!0,
                    border:!1,
                    showGroup:!0,
                    showHeader:!1
                });
            // });
        }
    };
})(jQuery);