<?php

use app\assets\DoriIndexAsset;
use yii\web\View;
use yii\helpers\Url;
?>

<?php $this->beginPage();?><?php $this->head();?><?php $this->beginBody();?>
<div class="easyui-layout" id="dori-index" fit="true">
    <div region="west" style="width:50%" split="true" iconCls="icon-ok" title="Дори номлари">
        <div id="dori-index-tb">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dori-index-dg').edatagrid('addRow')">Қўшиш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dori-index-dg').edatagrid('destroyRow')">Ўчириш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dori-index-dg').edatagrid('saveRow')">Сақлаш</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dori-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>
        </div>
        <div id="dori-index-dg"></div>
    </div>
    <div region="center">
        <div class="easyui-layout" data-options="fit:true">
            <div region="north" split="true" style="height:50%;" iconCls="icon-redo" title="Қўшилган дорилар">
                    <div id="dori-prixod-index-tb">
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dori-prixod-index-dg').edatagrid('addRow')">Қўшиш</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dori-prixod-index-dg').edatagrid('destroyRow')">Ўчириш</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dori-prixod-index-dg').edatagrid('saveRow')">Сақлаш</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dori-prixod-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>
                    </div>
                <div id="dori-prixod-index-dg"></div>
            </div>
            <div region="center" split="true" style="height:50%;" iconCls="icon-undo" title="Ишлатилган дорилар">
                    <div id="dori-rasxod-index-tb">
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dori-rasxod-index-dg').edatagrid('addRow')">Қўшиш</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dori-rasxod-index-dg').edatagrid('destroyRow')">Ўчириш</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dori-rasxod-index-dg').edatagrid('saveRow')">Сақлаш</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dori-rasxod-index-dg').edatagrid('cancelRow')">Бекор қилиш</a>
                    </div>
                <div id="dori-rasxod-index-dg"></div>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody();?>
<?php $this->endPage();?>
<?php
$newUrl = Url::to(['dori/new'],true);
$saveUrl = Url::to(['dori/save'],true);
$updateUrl = Url::to(['dori/update'],true);
$destroyUrl = Url::to(['dori/destroy'],true);
$savePUrl = Url::to(['dori/psave'],true);
$updatePUrl = Url::to(['dori/pupdate'],true);
$destroyPUrl = Url::to(['dori/pdestroy'],true);
$saveRUrl = Url::to(['dori/rsave'],true);
$updateRUrl = Url::to(['dori/rupdate'],true);
$destroyRUrl = Url::to(['dori/rdestroy'],true);
$deleteUrl = Url::to(['dori/delete'],true);
$getListDataUrl = Url::to(['dori/get-list-data'],true);
$getPrixodListDataUrl = Url::to(['dori/get-prixod-list-data'],true);
$getRasxodListDataUrl = Url::to(['dori/get-rasxod-list-data'],true);
$this->registerJs(<<<EOD
    yii.doriIndex.getListDataUrl = '{$getListDataUrl}';
    yii.doriIndex.getPrixodListDataUrl = '{$getPrixodListDataUrl}';
    yii.doriIndex.getRasxodListDataUrl = '{$getRasxodListDataUrl}';
//    yii.doriIndex.newUrl = '{$newUrl}';
    yii.doriIndex.savePUrl = '{$savePUrl}';
    yii.doriIndex.updatePUrl = '{$updatePUrl}';
    yii.doriIndex.destroyPUrl = '{$destroyPUrl}';
    yii.doriIndex.saveRUrl = '{$saveRUrl}';
    yii.doriIndex.updateRUrl = '{$updateRUrl}';
    yii.doriIndex.destroyRUrl = '{$destroyRUrl}';
    yii.doriIndex.saveUrl = '{$saveUrl}';
    yii.doriIndex.updateUrl = '{$updateUrl}';
    yii.doriIndex.destroyUrl = '{$destroyUrl}';
    yii.doriIndex.deleteUrl = '{$deleteUrl}';
    yii.doriIndex.init();
    yii.easyui.hideMainMask();
EOD
    , View::POS_END );

DoriIndexAsset::register($this);